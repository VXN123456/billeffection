package vienuser.example.com.billeffection;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import vienuser.example.com.billeffection.models.CustomerModel;
import vienuser.example.com.billeffection.models.Utils;

/**
 * Created by VienUser on 5/4/2016.
 */
public class CustomAdapterCustomer extends ArrayAdapter implements Filterable {

    List<CustomerModel> customerModels1;
    public List<CustomerModel> customerModelList;
    LayoutInflater inflater;
    int resource;
    CreateBill content;
    public CustomAdapterCustomer(CreateBill content, int resource, List<CustomerModel> objects) {
        super(content, resource,objects);
        this.resource = resource;
        this.content=content;
        this.customerModelList = objects;
        inflater = (LayoutInflater) content.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public Filter getFilter() {
        return new Filter() {

            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                final FilterResults oReturn = new FilterResults();
                final ArrayList<CustomerModel> results = new ArrayList<CustomerModel>();
                if(constraint=="" || constraint==null)
                    constraint="";
                if (customerModels1 == null)
                    customerModels1 =  customerModelList;
                if (constraint != null) {
                    if (customerModels1 != null && customerModels1.size() > 0) {
                        for (CustomerModel customerModel : customerModels1) {
                            if (Utils.ConvertString(customerModel.getTenKH().toLowerCase())
                                    .contains(Utils.ConvertString(constraint.toString()))
                                    || customerModel.getSdt().contains(constraint.toString()))
                                results.add(customerModel);
                        }
                    }
                    oReturn.values = results;
                    oReturn.count=results.size();
                }
                return oReturn;
            }

            @SuppressWarnings("unchecked")
            @Override
            protected void publishResults(CharSequence constraint,
                                          FilterResults results) {
                customerModelList = (ArrayList<CustomerModel>) results.values;
                notifyDataSetChanged();
            }
        };
    }


    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return customerModelList.size();
    }

    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return String.valueOf(customerModelList.get(position).getTenKH());
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return position;
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if (convertView == null)
        {
            convertView = inflater.inflate(resource,null);
        }
        TextView tvCustomerName;
        TextView tvSDT;
        TextView tvCode;
        TextView tvAddress;


        tvCustomerName = (TextView)convertView.findViewById(R.id.tv_NameCustomer);
        tvSDT = (TextView)convertView.findViewById(R.id.tv_PhoneCustomer);
        tvCode = (TextView)convertView.findViewById(R.id.tv_CustomerCode);
        tvAddress = (TextView)convertView.findViewById(R.id.tv_CustomerAddress);


        tvCustomerName.setText(customerModelList.get(position).getTenKH());
        tvSDT.setText(String.valueOf(customerModelList.get(position).getSdt()));
        tvCode.setText(String.valueOf(Integer.parseInt(customerModelList.get(position).getMaKH())));
        tvAddress.setText(customerModelList.get(position).getDiaChi());

        return convertView;
    }

}

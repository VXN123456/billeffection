package vienuser.example.com.billeffection;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

import vienuser.example.com.billeffection.models.BillDetailModel;
import vienuser.example.com.billeffection.models.Utils;

/**
 * Created by KP on 12/05/2016.
 */
public class CustomAdapterProduct_BillDetail extends ArrayAdapter {

    private LayoutInflater inflater;
    int resource;
    //int quantity;

    List<BillDetailModel> billDetailModelList;
    public CustomAdapterProduct_BillDetail(Context context, int resource, List<BillDetailModel> billDetailModelList ) {
        super(context, resource, billDetailModelList);
        this.resource = resource;
        this.billDetailModelList=billDetailModelList;
        inflater = (LayoutInflater) context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
    }

    //filter data



    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return billDetailModelList.size();
    }

    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return position;
    }


    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        if (convertView == null)
        {
            convertView = inflater.inflate(resource,null);
        }


        TextView tvNameSP = (TextView)convertView.findViewById(R.id.tv_ProductName);
        TextView tvPriceSP = (TextView)convertView.findViewById(R.id.tv_ProductPrice);
        TextView tvQuantity = (TextView)convertView.findViewById(R.id.tv_Quantity);
        TextView tvSTT = (TextView)convertView.findViewById(R.id.tv_STT) ;
        TextView tvPriceSemiFinal = (TextView)convertView.findViewById(R.id.tv_PricePerProduct) ;
        TextView tvCK = (TextView)convertView.findViewById(R.id.tv_ChiecKhau);
        TextView tvGG = (TextView)convertView.findViewById(R.id.tv_GG);

        tvSTT.setText(String.valueOf(position+1));
        tvGG.setText(Utils.ConvertCurrency(String.valueOf(Math.round(billDetailModelList.get(position).getGiamGia()))));
        tvQuantity.setText(String.valueOf(Math.round(billDetailModelList.get(position).getSoLuong())));
        tvNameSP.setText(billDetailModelList.get(position).getTenSanPham());
        tvCK.setText(String.valueOf(Math.round(billDetailModelList.get(position).getChietKhau())));
        tvPriceSP.setText(Utils.ConvertCurrency(String.valueOf(Math.round(billDetailModelList.get(position).getDonGia()))));
        tvPriceSemiFinal.setText(Utils.ConvertCurrency(String.valueOf(Math.round(billDetailModelList.get(position).getDonGia()*billDetailModelList.get(position).getSoLuong()-
                (billDetailModelList.get(position).getSoLuong()*billDetailModelList.get(position).getGiamGia())-
                (billDetailModelList.get(position).getDonGia()*billDetailModelList.get(position).getSoLuong()*billDetailModelList.get(position).chietKhau/100)))));

        return convertView;
    }
}


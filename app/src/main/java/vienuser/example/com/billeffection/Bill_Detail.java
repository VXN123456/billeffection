package vienuser.example.com.billeffection;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.Log;
import android.view.MenuItem;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import vienuser.example.com.billeffection.models.BillDetailModel;
import vienuser.example.com.billeffection.models.BillModel;
import vienuser.example.com.billeffection.models.CustomerModel;
import vienuser.example.com.billeffection.models.Utils;

/**
 * Created by KP on 12/05/2016.
 */
public class Bill_Detail  extends AppCompatActivity {

    private ListView lvItemSave;
    private ArrayList<BillDetailModel> lBillDetail;
    private CustomAdapterProduct_BillDetail savedAdapterProduct;
    private TextView tvSumPriceFinal;
    private TextView tvCustomerName;
    private TextView tvCustomerPhone;
    private TextView tvCustomerAddress;
    private TextView tvDate;
    private EditText etNote;
    private TextView tvCKFinal;
    private EditText etCustomerPay;
    BillModel bill;
    CustomerModel cus;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_save_bill_success);
        Toolbar toolbar = (Toolbar) findViewById(R.id.app_bar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
//        this.setTitle(getResources().getString(R.string.billDetail));
        this.setTitle(Html.fromHtml("<font color = '#131313'>Chi tiết hóa đơn</font>"));
        tvSumPriceFinal = (TextView)findViewById(R.id.tv_SumPriceFinal);
        tvCustomerName = (TextView)findViewById(R.id.tv_CustomerName);
        tvCustomerPhone = (TextView)findViewById(R.id.tv_CustomerPhone);
        tvCustomerAddress = (TextView)findViewById(R.id.tv_CustomerAddress);
        tvCKFinal = (TextView)findViewById(R.id.tv_CKFinal);
        etCustomerPay = (EditText)findViewById(R.id.et_CustomerPay);
        lvItemSave = (ListView)findViewById(R.id.lv_ItemSave);
        tvDate = (TextView)findViewById(R.id.tv_DataCreate) ;
        etNote = (EditText)findViewById(R.id.et_Note);
        etNote.setKeyListener(null);
        etCustomerPay.setKeyListener(null);
        Button btnSaveOnDataBase = (Button)findViewById(R.id.btn_SaveFinal);
        ViewGroup layout = (ViewGroup)btnSaveOnDataBase.getParent();
        layout.removeView(btnSaveOnDataBase);

        Intent i = getIntent();
        bill = (BillModel) i.getSerializableExtra("bill");
        //set
        tvSumPriceFinal.setText(Utils.ConvertCurrency("0"));
        tvCustomerName.setText(bill.getTenKhachHang());
        tvDate.setText(Utils.ChangeDateFormat(bill.getNgayLap()));
        etNote.setText(" "+bill.getGhiChu());
        etCustomerPay.setText(Utils.ConvertCurrency(bill.getSoTienKhachDua().toString()));
        tvCKFinal.setText(bill.getChietKhau().toString());
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == android.R.id.home) {
            this.finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
    @Override
    public void onResume()
    {  // After a pause OR at startup
        super.onResume();

        //check network
        Utils.context=this;
        if(!Utils.isConnectingToInternet()) {
            final AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setMessage(getResources().getString(R.string.requiredConnect))
                    .setPositiveButton(getResources().getString(R.string.retry), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            onResume();
                        }
                    })
                    .setNegativeButton(getResources().getString(R.string.close), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            dialogInterface.dismiss();
                        }
                    }).show();
        }
        else
        {
            RequestItemsServiceTask task = new RequestItemsServiceTask();
            task.execute();
        }
    }

    private class RequestItemsServiceTask extends AsyncTask<Void, Void, Void> {

        private ProgressDialog dialog = new ProgressDialog(Bill_Detail.this);
        String stringJSONbilldetail = null;
        String stringJSONcus = null;
        @Override
        protected void onPreExecute() {
            // TODO i18n
            dialog.setMessage(getResources().getString(R.string.please_wait));
            dialog.setButton(DialogInterface.BUTTON_NEGATIVE, getResources().getString(R.string.close), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();

                }
            });
            dialog.show();
        }

        @Override
        protected Void doInBackground(Void... unused) {
            // The ItemService would contain the method showed
            // in the previous paragraph
            String surlbilldetail = "http://salemh.skyit.vn/api/ChiTietHoaDon/" + bill.getId();
            String surlcus = "http://salemh.skyit.vn/api/KhachHang/" + bill.getIdKhachHang();
            Log.d("JSONCUS:",surlcus);
            try {
                stringJSONbilldetail = Utils.makeRequest2("GET", surlbilldetail, null, "application/json", "");
                stringJSONcus = Utils.makeRequest2("GET", surlcus, null, "application/json", "");
                Log.d("RESULT:", stringJSONbilldetail);
            } catch (IOException e) {
                e.printStackTrace();
                stringJSONbilldetail = null;
                stringJSONcus = null;
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void unused) {
            if (stringJSONbilldetail != null && stringJSONcus != null) {
                lBillDetail=new ArrayList<BillDetailModel>();
                try {
                    JSONArray jsonArray = new JSONArray(stringJSONbilldetail);
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject jsonObject = jsonArray.getJSONObject(i);
                        try {
                            BillDetailModel billDetail = new BillDetailModel();
                            billDetail.setIdSanPham(jsonObject.getInt("idSanPham"));
                            billDetail.setIdHoaDon(jsonObject.getInt("idHoaDon"));
                            billDetail.setChietKhau((float) jsonObject.getDouble("chietKhau"));
                            billDetail.setGiamGia(jsonObject.getInt("giamGia"));
                            billDetail.setSoLuong(jsonObject.getInt("soLuong"));
                            billDetail.setTenSanPham(jsonObject.getString("tenSanPham"));
                            billDetail.setDonGia((float) jsonObject.getDouble("donGia"));
                            lBillDetail.add(billDetail);



                        } catch (JSONException e) {
                            continue;
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                cus = new CustomerModel();
                try {
                    JSONArray jsonArray = new JSONArray(stringJSONcus);
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject jsonObject = jsonArray.getJSONObject(i);
                        try {
                            cus.setId(jsonObject.getInt("id"));
                            cus.setMaKH(String.valueOf(jsonObject.getInt("maKhachHang")));
                            cus.setTenKH(jsonObject.getString("tenKhachHang"));
                            cus.setDiaChi(jsonObject.getString("diaChi"));
                            cus.setEmail(jsonObject.getString("email"));
                            cus.setSdt(jsonObject.getString("soDienThoai"));
                            cus.setSoFax(jsonObject.getString("sofax"));
                            cus.setMsThue(jsonObject.getString("maSoThue"));
                        } catch (JSONException e) {
                            continue;
                        }
                    }
                }
                catch (JSONException e)
                {
                    e.printStackTrace();
                }

                tvSumPriceFinal.setText(Utils.ConvertCurrency(String.valueOf(Math.round(bill.getTongTien()))));
                tvCustomerAddress.setText(cus.getDiaChi());
                tvCustomerPhone.setText(cus.getSdt());
                savedAdapterProduct = new CustomAdapterProduct_BillDetail(Bill_Detail.this, R.layout.row_item_save,lBillDetail);
                lvItemSave.setAdapter(savedAdapterProduct);

                if (dialog.isShowing()) {
                    dialog.dismiss();
                }
            } else
                dialog.setMessage(getResources().getString(R.string.requiredServer));
        }

    }
}

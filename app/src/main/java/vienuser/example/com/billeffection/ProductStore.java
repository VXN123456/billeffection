package vienuser.example.com.billeffection;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.text.TextUtils;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.SearchView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import vienuser.example.com.billeffection.models.ProductModel;
import vienuser.example.com.billeffection.models.ProductTypeModel;
import vienuser.example.com.billeffection.models.Utils;

public class ProductStore extends AppCompatActivity {
    SearchView sv;
    ListView lv;
    CustomAdapter_Product productAdapter;
    ArrayList<ProductTypeModel> productTypeList;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_store);
        Toolbar toolbar = (Toolbar) findViewById(R.id.app_bar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        this.setTitle(Html.fromHtml("<font color='#131313'>Danh sách sản phẩm</font>"));
//        this.setTitle(getResources().getString(R.string.tv_ListProduct));
        sv = (SearchView)findViewById(R.id.sv_product);
        lv = (ListView)findViewById(R.id.lv_product);

        //Checkconnection
        CheckConnection();
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == android.R.id.home) {
            this.finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
    public void CheckConnection()
    {
        //check network
        Utils.context=this;
        if(!Utils.isConnectingToInternet()) {
            final AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setMessage(getResources().getString(R.string.requiredConnect))
                    .setPositiveButton(getResources().getString(R.string.retry), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            CheckConnection();
                        }
                    })
                    .setNegativeButton(getResources().getString(R.string.close), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            dialogInterface.dismiss();
                        }
                    }).show();
        }
        else
        {
            RequestItemsServiceTask task = new RequestItemsServiceTask();
            task.execute();
        }
    }
    private class RequestItemsServiceTask extends AsyncTask<Void, Void, Void> {
        private ProgressDialog dialog = new ProgressDialog(ProductStore.this);
        private ArrayList<ProductModel> productList;
        String stringJSONsp=null;
        String stringJSONlsp=null;

        @Override
        protected void onPreExecute() {
            // TODO i18n
            dialog.setMessage(getResources().getString(R.string.please_wait));
            dialog.setButton(DialogInterface.BUTTON_NEGATIVE, getResources().getString(R.string.close), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();

                }
            });
            dialog.show();
        }

        @Override
        protected Void doInBackground(Void... unused) {
            // The ItemService would contain the method showed
            // in the previous paragraph
            String surlsp = "http://salemh.skyit.vn/api/SanPham/";
            String surllsp="http://salemh.skyit.vn/api/LoaiSanPham/";
            try {
                stringJSONsp= Utils.makeRequest2("GET",surlsp,null,"application/json","");
                stringJSONlsp= Utils.makeRequest2("GET",surllsp,null,"application/json","");

                Log.d("RESULT1:",stringJSONsp);
                Log.d("RESULT2:",stringJSONlsp);
            } catch (IOException e) {
                stringJSONsp=null;
                stringJSONlsp=null;
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void unused) {
            if(stringJSONlsp!=null && stringJSONsp!=null) {
                productList = new ArrayList<ProductModel>();
                try {
                    JSONArray jsonArray = new JSONArray(stringJSONsp);
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject jsonObject = jsonArray.getJSONObject(i);
                        try {
                            ProductModel product = new ProductModel();
                            product.setId(jsonObject.getInt("id"));
                            product.setTenSanPham(jsonObject.getString("tenSanPham"));
                            product.setMaSanPham(jsonObject.getString("maSanPham"));
                            product.setGiaBan((float) jsonObject.getDouble("giaBan"));
                            product.setDonVi(jsonObject.getString("donVi"));
                            product.setIdLoaiSanPham(jsonObject.getInt("idLoaiSanPham"));
                            product.setMoTa(jsonObject.getString("moTa"));
                            product.setSoLuong((float) jsonObject.getDouble("soLuong"));
                            product.setGiaSiCapMot((float) jsonObject.getDouble("giaSiCapMot"));
                            productList.add(product);
                        } catch (JSONException e) {
                            continue;
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                productTypeList = new ArrayList<ProductTypeModel>();
                try {
                    JSONArray jsonArray = new JSONArray(stringJSONlsp);
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject jsonObject = jsonArray.getJSONObject(i);
                        try {
                            ProductTypeModel producttype = new ProductTypeModel();
                            producttype.setId(jsonObject.getInt("id"));
                            producttype.setTenLoaiSanPham(jsonObject.getString("tenLoaiSanPham"));
                            productTypeList.add(producttype);
                        } catch (JSONException e) {
                            continue;
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                if(productList.size() != 0 && productTypeList.size()!=0) {
                    Collections.sort(productList, new Comparator<ProductModel>() {
                        @Override
                        public int compare(ProductModel type1, ProductModel type2) {
                            return Integer.parseInt(type1.getMaSanPham()) - Integer.parseInt(type2.getMaSanPham());
                        }
                    });

                    productAdapter = new CustomAdapter_Product(ProductStore.this, productList, productTypeList);
                    lv.setAdapter(productAdapter);
                    lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {

                            ProductDetail dialog = new ProductDetail();
                            FragmentManager fragmentManager = getSupportFragmentManager();
                            getIntent().putExtra("product", productAdapter.lproduct.get(position));
                            getIntent().putExtra("producttype", productTypeList);
                            dialog.show(fragmentManager, "");
                        }
                    });
                    sv.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
                        android.widget.Filter filter = productAdapter.getFilter();

                        @Override
                        public boolean onQueryTextSubmit(String query) {
                            return false;
                        }

                        @Override
                        public boolean onQueryTextChange(String newText) {
                            if (TextUtils.isEmpty(newText)) {
                                filter.filter("");
                            } else {
                                //lv.setFilterText(newText);
                                filter.filter(newText);
                            }
                            return true;
                        }
                    });
//            lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//                @Override
//                public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
//                    Toast.makeText(getApplication(), "You Clicked " + i, Toast.LENGTH_LONG).show();
//                }
//            });
                    if (dialog.isShowing()) {
                        dialog.dismiss();
                    }
                }
                dialog.setMessage(getResources().getString(R.string.empty_data));
            }
            else
                dialog.setMessage(getResources().getString(R.string.requiredServer));
        }
    }
}

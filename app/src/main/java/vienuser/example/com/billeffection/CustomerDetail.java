package vienuser.example.com.billeffection;

import android.app.Dialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;

import org.json.JSONObject;

import vienuser.example.com.billeffection.models.CustomerModel;

/**
 * Created by KP on 06/05/2016.
 */
public class CustomerDetail extends DialogFragment {
    Dialog dialog;
    Button bt_ok;
    Button bt_cancel;
    JSONObject params;
    EditText et_tenkh;
    EditText et_diachi;
    EditText et_sdt;
    EditText et_sofax;
    EditText et_msthue;
    ImageButton ibt_call;
    CustomerModel customer;
    CustomerStore context;
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Intent i = getActivity().getIntent();
        customer = i.getParcelableExtra("customer");
        dialog = new Dialog(getActivity(),android.R.style.Theme_Holo_Light_Dialog);
        dialog.setTitle(getResources().getString(R.string.customerDetail));
        //dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);


        dialog.setContentView(R.layout.dialog_customer_detail);
        bt_ok = (Button) dialog.findViewById(R.id.bt_ok);
        bt_cancel=(Button)dialog.findViewById(R.id.bt_cancel);
        et_tenkh=(EditText)dialog.findViewById(R.id.et_tenkh);
        et_diachi=(EditText)dialog.findViewById(R.id.et_diachi);
        et_sdt=(EditText)dialog.findViewById(R.id.et_sdt);
        et_sofax=(EditText)dialog.findViewById(R.id.et_sofax);
        et_msthue=(EditText)dialog.findViewById(R.id.et_msthue);
        ibt_call=(ImageButton)dialog.findViewById(R.id.ibt_call);
        //et_tenkh.setEnabled(false);
        et_tenkh.setKeyListener(null); // tắt nhận hoạt động từ bàn phím
        et_tenkh.setTextIsSelectable(true); // bật cho phép select text
        et_diachi.setKeyListener(null);
        et_sdt.setKeyListener(null);
        et_sofax.setKeyListener(null);
        et_msthue.setKeyListener(null);
        ibt_call.setVisibility(View.VISIBLE);

        et_tenkh.setText(customer.getTenKH());
        et_diachi.setText(customer.getDiaChi());
        et_sdt.setText(customer.getSdt());
        et_sofax.setText(customer.getSoFax());
        et_msthue.setText(customer.getMsThue());
        ibt_call.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String url = "tel:"+String.valueOf(et_sdt.getText());
                Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse(url));
                startActivity(intent);
            }
        });
        ViewGroup layout = (ViewGroup)bt_ok.getParent();
        layout.removeView(bt_ok);

        bt_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
        return dialog;
    }
}

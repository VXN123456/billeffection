package vienuser.example.com.billeffection;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import vienuser.example.com.billeffection.models.DBHelper;
import vienuser.example.com.billeffection.models.UserModel;
import vienuser.example.com.billeffection.models.Utils;

public class Login extends AppCompatActivity {


    private EditText et_username;
    private EditText et_pass;
    private CheckBox cb_save;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        Button btn_Login = (Button)findViewById(R.id.btn_Login);
        et_username = (EditText)findViewById(R.id.et_Username);
        et_pass = (EditText)findViewById(R.id.et_Password);
        cb_save=(CheckBox)findViewById(R.id.cb_save);
        if (btn_Login != null) {
            btn_Login.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Utils.context=Login.this;
                    if(!Utils.isConnectingToInternet()) {
                        final AlertDialog.Builder builder = new AlertDialog.Builder(Login.this);
                        builder.setMessage("Không có mạng, vui lòng kết nối và thử lại")
                                .setPositiveButton(getResources().getString(R.string.retry), new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        onResume();
                                    }
                                }).show();
                    }
                    else
                    {
                        new LoginTask().execute("http://salemh.skyit.vn/api/NhanVien");
                    }


                }
            });
        }
    }



    public class LoginTask extends AsyncTask<String, String, List<UserModel>>{

        @Override
        protected List<UserModel> doInBackground(String... params) {

            HttpURLConnection connection;
            BufferedReader reader;

            try {
                URL url = new URL(params[0]);
                connection = (HttpURLConnection)url.openConnection();
                connection.connect();
                InputStream stream = connection.getInputStream();
                reader = new BufferedReader(new InputStreamReader(stream));
                StringBuffer buffer = new StringBuffer();

                String line;
                while ((line = reader.readLine())!= null)
                {
                    buffer.append(line);
                }
                String jsonFinal = buffer.toString();
                JSONArray jsonArrayParent = new JSONArray(jsonFinal);

                List<UserModel> userModelList = new ArrayList<>();
                for (int i = 0;i<jsonArrayParent.length();i++)
                {
                    JSONObject objectFinal = jsonArrayParent.getJSONObject(i);
                    try {
                        UserModel userModel = new UserModel();
                        userModel.setTenDangNhap(objectFinal.getString("tenDangNhap"));
                        userModel.setMatKhau(objectFinal.getString("matKhau"));
                        userModel.setId(objectFinal.getInt("id"));
                        userModel.setTenNV(objectFinal.getString("tenNhanVien"));

                        userModelList.add(userModel);
                    }
                    catch (JSONException e){
                        continue;
                    }

                }

                return userModelList;

            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(List<UserModel> userModels) {
            super.onPostExecute(userModels);

            boolean ok = false;
            for (UserModel userModel : userModels){
                if ((et_username.getText().toString()) .equals(userModel.getTenDangNhap()) && (et_pass.getText().toString()).equals( userModel.getMatKhau()))
                {
                    //Save login state
                    if(cb_save.isChecked()) {
                        DBHelper db = new DBHelper(getApplication());
                        db.saveState(userModel.getTenDangNhap(), userModel.getMatKhau());
                    }
                    int id = userModel.getId();
                    Intent intent_Login = new Intent(Login.this,LoginSuccess.class);
                    intent_Login.putExtra("data_Username",userModel.getTenDangNhap());
                    intent_Login.putExtra("data_Name",userModel.getTenNV());
                    intent_Login.putExtra("data_IdUser", String.valueOf(id));
                    finish();
                    startActivity(intent_Login);
                    ok = true;
                    break;
                }
            }
            if (ok == false){
                Toast.makeText(Login.this,"Username or Password is incorrect!!!",Toast.LENGTH_SHORT).show();
            }
        }

    }

}

package vienuser.example.com.billeffection;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import java.util.ArrayList;

import vienuser.example.com.billeffection.models.BillModel;
import vienuser.example.com.billeffection.models.Utils;

/**
 * Created by KP on 26/04/2016.
 */
public class CustomAdapter_Bill extends BaseAdapter implements Filterable {
    ArrayList<BillModel> lbill;
    ArrayList<BillModel> orig;
    Context context;
    private LayoutInflater inflater = null;

    public CustomAdapter_Bill(BillStore mainActivity, ArrayList<BillModel> al) {
        // TODO Auto-generated constructor stub
        lbill = al;
        context = mainActivity;
        inflater = (LayoutInflater) context.
                getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public Filter getFilter() {
        return new Filter() {

            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                final FilterResults oReturn = new FilterResults();
                final ArrayList<BillModel> results = new ArrayList<BillModel>();
                if (orig == null)
                    orig = lbill;
                if (constraint != null) {
                    if (orig != null && orig.size() > 0) {
                        for (final BillModel g : orig) {
                            if (Utils.ConvertString(g.getTenKhachHang().toString().toLowerCase()).contains(Utils.ConvertString(constraint.toString())) || Utils.ChangeDateFormat(g.getNgayLap()).contains(constraint.toString()))
                                results.add(g);
                        }
                    }
                    oReturn.values = results;
                }
                return oReturn;
            }

            @SuppressWarnings("unchecked")
            @Override
            protected void publishResults(CharSequence constraint,
                                          FilterResults results) {
                lbill = (ArrayList<BillModel>) results.values;
                notifyDataSetChanged();
            }
        };
    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return lbill.size();
    }

    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    public class Holder {
        TextView tvname;
        TextView tvid;
        TextView tvprice;
        TextView tvdate;
    }
    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub
        Holder holder = new Holder();
        if(convertView==null) {
            convertView = inflater.inflate(R.layout.custom_listview_bill_store, null);
        }
        holder.tvid = (TextView) convertView.findViewById(R.id.tv_id);
        holder.tvname = (TextView) convertView.findViewById(R.id.tv_name);
        holder.tvprice = (TextView) convertView.findViewById(R.id.tv_price);
        holder.tvdate = (TextView) convertView.findViewById(R.id.tv_date);
        holder.tvname.setText(lbill.get(position).getTenKhachHang().toString());
        holder.tvid.setText(lbill.get(position).getSoHoaDon().toString());
        holder.tvprice.setText(Utils.ConvertCurrency(lbill.get(position).getTongTien().toString()));


        holder.tvdate.setText("Ngày lập: "+Utils.ChangeDateFormat(lbill.get(position).getNgayLap()));

        return convertView;
    }
}

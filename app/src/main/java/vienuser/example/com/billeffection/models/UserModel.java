package vienuser.example.com.billeffection.models;

import java.io.Serializable;

/**
 * Created by VienUser on 4/27/2016.
 */
public class UserModel implements Serializable {

    private int id;
    private String tenDangNhap;
    private String TenNV;
    private String matKhau;
    private String chucVu;
    private String sdt;
    private boolean hopLe;
    private boolean toanQuyen;

    public boolean isToanQuyen() {
        return toanQuyen;
    }

    public void setToanQuyen(boolean toanQuyen) {
        this.toanQuyen = toanQuyen;
    }

    public int getId() {return id;}

    public void setId(int id) {
        this.id = id;
    }

    public String getTenDangNhap() {
        return tenDangNhap;
    }

    public void setTenDangNhap(String tenDangNhap) {
        this.tenDangNhap = tenDangNhap;
    }

    public String getTenNV() {
        return TenNV;
    }

    public void setTenNV(String tenNV) {
        TenNV = tenNV;
    }

    public String getMatKhau() {
        return matKhau;
    }

    public void setMatKhau(String matKhau) {
        this.matKhau = matKhau;
    }

    public String getChucVu() {
        return chucVu;
    }

    public void setChucVu(String chucVu) {
        this.chucVu = chucVu;
    }

    public String getSdt() {
        return sdt;
    }

    public void setSdt(String sdt) {
        this.sdt = sdt;
    }

    public boolean isHopLe() {
        return hopLe;
    }

    public void setHopLe(boolean hopLe) {
        this.hopLe = hopLe;
    }



}

package vienuser.example.com.billeffection;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.text.TextUtils;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.SearchView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import vienuser.example.com.billeffection.models.CustomerModel;
import vienuser.example.com.billeffection.models.Utils;

public class CustomerStore extends AppCompatActivity {
    SearchView sv;
    ListView lv;
    CustomAdapter_Customer customerAdapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_customer_store);
        Toolbar toolbar = (Toolbar) findViewById(R.id.app_bar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        this.setTitle(Html.fromHtml("<font color='#131313'>Danh sách khách hàng</font>"));
//        this.setTitle(getResources().getString(R.string.tv_ListCustomer));
        sv = (SearchView)findViewById(R.id.sv_customer);
        lv = (ListView)findViewById(R.id.lv_customer);

        //Checkconnection
        CheckConnection();
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == android.R.id.home) {
            this.finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
    public void CheckConnection()
    {
        //Check network
        Utils.context=this;
        if(!Utils.isConnectingToInternet()) {
            final AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setMessage(getResources().getString(R.string.requiredConnect))
                    .setPositiveButton(getResources().getString(R.string.retry), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            CheckConnection();
                        }
                    })
                    .setNegativeButton(getResources().getString(R.string.close), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            dialogInterface.dismiss();
                        }
                    }).show();
        }
        else
        {
            RequestItemsServiceTask task = new RequestItemsServiceTask();
            task.execute();
        }
    }
    private class RequestItemsServiceTask extends AsyncTask<Void, Void, Void> {
        private ProgressDialog dialog = new ProgressDialog(CustomerStore.this);
        private ArrayList<CustomerModel> customerList;
        String stringJSON=null;
        @Override
        protected void onPreExecute() {
            // TODO i18n
            dialog.setMessage(getResources().getString(R.string.please_wait));
            dialog.setButton(DialogInterface.BUTTON_NEGATIVE, getResources().getString(R.string.close), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();

                }
            });
            dialog.show();
        }

        @Override
        protected Void doInBackground(Void... unused) {
            // The ItemService would contain the method showed
            // in the previous paragraph
                String surl = "http://salemh.skyit.vn/api/KhachHang/";
                try {
                    stringJSON = Utils.makeRequest2("GET", surl, null, "application/json", "");
                    Log.d("RESULT:", stringJSON);
                } catch (IOException e) {
//                e.printStackTrace();
                    stringJSON = null;
                    Log.d("RESULT:", "null");
                }
            return null;
        }

        @Override
        protected void onPostExecute(Void unused) {
            if (stringJSON != null) {
                customerList = new ArrayList<CustomerModel>();
                try {
                    JSONArray jsonArray = new JSONArray(stringJSON);
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject jsonObject = jsonArray.getJSONObject(i);
                        try {
                            CustomerModel customer = new CustomerModel();
                            customer.setId(jsonObject.getInt("id"));
                            customer.setMaKH(String.valueOf(jsonObject.getInt("maKhachHang")));
                            customer.setTenKH(jsonObject.getString("tenKhachHang"));
                            customer.setDiaChi(jsonObject.getString("diaChi"));
                            customer.setEmail(jsonObject.getString("email"));
                            customer.setSdt(jsonObject.getString("soDienThoai"));
                            customer.setSoFax(jsonObject.getString("soFAX"));
                            customer.setMsThue(jsonObject.getString("maSoThue"));
                            customerList.add(customer);
                        } catch (JSONException e) {
                            continue;
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                if(customerList.size()!=0) {
                    customerAdapter = new CustomAdapter_Customer(CustomerStore.this, customerList);
                    lv.setAdapter(customerAdapter);
                    lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {

                            CustomerDetail dialog = new CustomerDetail();
                            FragmentManager fragmentManager = getSupportFragmentManager();
                            getIntent().putExtra("customer", customerAdapter.lCus.get(position));
                            dialog.show(fragmentManager, "");
                        }
                    });
                    sv.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
                        android.widget.Filter filter = customerAdapter.getFilter();

                        @Override
                        public boolean onQueryTextSubmit(String query) {
                            return false;
                        }

                        @Override
                        public boolean onQueryTextChange(String newText) {
                            if (TextUtils.isEmpty(newText)) {
                                filter.filter("");
                            } else {
                                //lv.setFilterText(newText);
                                filter.filter(newText);
                            }
                            return true;
                        }
                    });
                    if (dialog.isShowing()) {
                        dialog.dismiss();
                    }
                }
                else
                    dialog.setMessage(getResources().getString(R.string.empty_data));
            } else {
                dialog.setMessage(getResources().getString(R.string.requiredServer));

            }
        }
    }
}
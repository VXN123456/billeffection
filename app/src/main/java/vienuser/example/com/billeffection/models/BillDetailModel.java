package vienuser.example.com.billeffection.models;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by KP on 12/05/2016.
 */
public class BillDetailModel implements Parcelable {
    public int id ;
    public int idHoaDon ;
    public float soLuong ;
    public float donGia ;
    public int idSanPham ;
    public String tenSanPham;
    public float giaVon;
    public float chietKhau;
    public String ghiChu;
    public int giamGia;

    public BillDetailModel() {
        id = 0;
        idHoaDon = 0;
        soLuong = 0;
        donGia = 0;
        idSanPham = 0;
        giaVon = 0;
        chietKhau = 0;
        ghiChu = "";
        giamGia = 0;
    }

    protected BillDetailModel(Parcel in) {
        id = in.readInt();
        idHoaDon = in.readInt();
        soLuong = in.readFloat();
        donGia = in.readFloat();
        idSanPham = in.readInt();
        tenSanPham = in.readString();
        giaVon = in.readFloat();
        chietKhau = in.readFloat();
        ghiChu = in.readString();
        giamGia = in.readInt();
    }

    public static final Creator<BillDetailModel> CREATOR = new Creator<BillDetailModel>() {
        @Override
        public BillDetailModel createFromParcel(Parcel in) {
            return new BillDetailModel(in);
        }

        @Override
        public BillDetailModel[] newArray(int size) {
            return new BillDetailModel[size];
        }
    };

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getIdHoaDon() {
        return idHoaDon;
    }

    public void setIdHoaDon(int idHoaDon) {
        this.idHoaDon = idHoaDon;
    }

    public float getSoLuong() {
        return soLuong;
    }

    public void setSoLuong(float soLuong) {
        this.soLuong = soLuong;
    }

    public float getDonGia() {
        return donGia;
    }

    public void setDonGia(float donGia) {
        this.donGia = donGia;
    }

    public int getIdSanPham() {
        return idSanPham;
    }

    public void setIdSanPham(int idSanPham) {
        this.idSanPham = idSanPham;
    }

    public String getTenSanPham() {
        return tenSanPham;
    }

    public void setTenSanPham(String tenSanPham) {
        this.tenSanPham = tenSanPham;
    }

    public float getGiaVon() {
        return giaVon;
    }

    public void setGiaVon(float giaVon) {
        this.giaVon = giaVon;
    }

    public float getChietKhau() {
        return chietKhau;
    }

    public void setChietKhau(float chietKhau) {
        this.chietKhau = chietKhau;
    }

    public String getGhiChu() {
        return ghiChu;
    }

    public void setGhiChu(String ghiChu) {
        this.ghiChu = ghiChu;
    }

    public int getGiamGia() {
        return giamGia;
    }

    public void setGiamGia(int giamGia) {
        this.giamGia = giamGia;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(id);
        parcel.writeInt(idHoaDon);
        parcel.writeFloat(soLuong);
        parcel.writeFloat(donGia);
        parcel.writeInt(idSanPham);
        parcel.writeString(tenSanPham);
        parcel.writeFloat(giaVon);
        parcel.writeFloat(chietKhau);
        parcel.writeString(ghiChu);
        parcel.writeInt(giamGia);
    }
}

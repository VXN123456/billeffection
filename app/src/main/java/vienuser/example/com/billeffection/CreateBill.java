package vienuser.example.com.billeffection;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import vienuser.example.com.billeffection.ProductEdit.EditNameDialogListener;
import vienuser.example.com.billeffection.models.CustomerModel;
import vienuser.example.com.billeffection.models.ProductModel;
import vienuser.example.com.billeffection.models.Utils;


public class CreateBill extends AppCompatActivity implements EditNameDialogListener {
    private ListView listViewProduct;
    private SearchView svProduct;
    private String customerName="";
    private String customerId="";
    private String customerAddress="";
    private String customerPhone="";
    private String productName = null;
    private int productQuantity = 1;
    private float productCK = 0;
    private int productGG = 0;
    private float productID = 0;
    private float productPrice = 0;
    private String productCode = null;
    public static ArrayList<ProductModel> productModels;
    public static TextView tvSumPrice;
    private CustomAdapterProduct productBill;
    public static int sumPrice = 0;
    public static boolean isExistSearchCustomer = false;
    public static boolean isExistSearchProduct = false;
    boolean iSReloadSTT = false;
    FragmentManager fragmentManager;
    List<CustomerModel> searchCustomerModels;
    List<ProductModel> searchProductModels;
    public EditText etSL;
    public EditText etPrice;
    private EditText etCK;
    private TextView tvQuantityTotal;
    private EditText etCKFinal;
    private TextView tvFinalPr;
    private ListView lvBillCreate;
    public static EditText etGG;
    private String pName;
    private int sl;
    private float ck;
    private int gg;
    private float dg;
    private int position;
    private String slEdited;
    private String dgEdited;
    private String ckEdited;
    private String ggEdited;
    private TextView tvOptionPrice;
    public static String optionPrice;
    public static  int price;

    public String getpName() {
        return pName;
    }

    public void setpName(String pName) {
        this.pName = pName;
    }

    public int getSl() {
        return sl;
    }
    public void setSl(int sl) {
        this.sl = sl;
    }

    public float getCk() {
        return ck;
    }

    public void setCk(float ck) {
        this.ck = ck;
    }

    public int getGg() {
        return gg;
    }

    public void setGg(int gg) {
        this.gg = gg;
    }

    public float getDg() {
        return dg;
    }

    public void setDg(float dg) {
        this.dg = dg;
    }

    AutoCompleteTextView attv_Product;
    public static AutoCompleteTextView attv_Customer;
    CustomAdapterCustomer adapter_Customer;
    customadapter adapter_Product;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setTitle(getResources().getString(R.string.createBill));
        setContentView(R.layout.activity_create_bill);
        Toolbar toolbar = (Toolbar) findViewById(R.id.app_bar_create_bill);
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


        fragmentManager = getSupportFragmentManager();

        productModels = new ArrayList<>();
        lvBillCreate = (ListView) findViewById(R.id.lv_BillCreate);
        tvSumPrice = (TextView)findViewById(R.id.tv_SumPrice);
        tvQuantityTotal = (TextView)findViewById(R.id.tv_QuantityTotal);
        etSL = (EditText)findViewById(R.id.et_Quantity);
        etCK = (EditText)findViewById(R.id.et_CK);
        etPrice = (EditText)findViewById(R.id.et_Price);
        attv_Customer = (AutoCompleteTextView)findViewById(R.id.attv_Customer) ;
        attv_Product=(AutoCompleteTextView)findViewById(R.id.attv_Product);
        etCKFinal = (EditText)findViewById(R.id.et_CKFinal);
        tvFinalPr = (TextView)findViewById(R.id.tv_FinalPr) ;
        etGG = (EditText)findViewById(R.id.et_GG);
        if (etGG != null) {
            etGG.setSelectAllOnFocus(true);
        }
        tvOptionPrice = (TextView)findViewById(R.id.tv_optionPrice);

        etCKFinal.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }
            @Override
            public void afterTextChanged(Editable s) {
                if (!tvSumPrice.getText().toString().equals("0")){
                    float ip;
                    if (etCKFinal.getText().length()>0&& (!etCKFinal.getText().toString().equals("."))) {
                         ip = Float.parseFloat((etCKFinal.getText().toString()))/100;
                    }
                    else {
                        ip = 0;
                    }
                    tvFinalPr.setText(Utils.ConvertCurrency(String.valueOf((int)(sumPrice-sumPrice*ip))));
                }
                else
                {
                    tvFinalPr.setText(Utils.ConvertCurrency("0"));
                }
            }
        });

        Button btnSave = (Button)findViewById(R.id.btn_Save);
        Button btnCancel = (Button)findViewById(R.id.btn_Cancel);
        ImageButton btnSearchCustomer = (ImageButton)findViewById(R.id.btn_SearchCustomer);
        ImageButton btnSearchProduct = (ImageButton)findViewById(R.id.btn_SearchProduct);
        Button btnChooseProduct = (Button)findViewById(R.id.btn_ChooseProduct);

        //Create bill cancel
        if (btnCancel != null) {
            btnCancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(CreateBill.this);
                    builder.setTitle(getResources().getString(R.string.recreate_bill))
                            .setPositiveButton(getResources().getString(R.string.yes), new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    productModels.clear();
                                    onResume();
                                    productBill.notifyDataSetChanged();

                                }
                            })
                            .setNegativeButton(getResources().getString(R.string.no), new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    dialogInterface.dismiss();
                                }
                            }).show();
                }
            });
        }

        if (btnSearchCustomer != null) {
            btnSearchCustomer.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (!isExistSearchCustomer){
                        SearchCustomer searchCustomer = new SearchCustomer();
                        Bundle bundle = new Bundle();
                        bundle.putParcelableArrayList("data_Customer", (ArrayList<? extends Parcelable>) searchCustomerModels);
                        searchCustomer.setArguments(bundle);
                        FragmentTransaction transaction = fragmentManager.beginTransaction();
                        transaction.addToBackStack(null);
                        transaction.add(R.id.search_customer_fragment,searchCustomer,"searchCustomer_Tag");
                        transaction.commit();
                    }
                    else
                    {
                        removeFragmentSearchCustomer();
                        isExistSearchCustomer = false;
                    }
                }
            });
        }
        if (btnSearchProduct != null) {
            btnSearchProduct.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (!isExistSearchProduct){
                        SearchProduct searchProduct = new SearchProduct();
                        Bundle bundle = new Bundle();
                        bundle.putParcelableArrayList("data_Product", (ArrayList<? extends Parcelable>) searchProductModels);
                        searchProduct.setArguments(bundle);
                        FragmentTransaction transaction = fragmentManager.beginTransaction();
                        transaction.addToBackStack(null);
                        transaction.add(R.id.search_product_fragment,searchProduct,"searchProduct_Tag");
                        transaction.commit();
                    }
                    else
                    {
                        removeFragmentSearchProduct();
                        isExistSearchProduct = false;
                    }
                }
            });
        }
        //set value SL,price,CK

        if (btnChooseProduct!= null)
        {
            btnChooseProduct.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(getDataCustomerName().equals("")){
                        Toast.makeText(getApplicationContext(),"Vui lòng chọn khách hàng!",Toast.LENGTH_LONG).show();
                    }else {
                        if (attv_Product.getText().length()==0){
                            Toast.makeText(getApplicationContext(),"Vui lòng chọn sản phẩm !",Toast.LENGTH_LONG).show();
                        }
                        else {
                            if ((int)Float.parseFloat(etGG.getText().toString())> (int) Float.parseFloat(etPrice.getText().toString()))
                            {
                                Toast.makeText(getApplicationContext(),"Vui lòng nhập giảm giá nhỏ hơn đơn giá!",Toast.LENGTH_LONG).show();
                            }
                            else
                            {
                                String pName = attv_Product.getText().toString();
                                String pSL;
                                String pCK;
                                String pGG;
                                String pPrice;
                                if (etSL.getText().length()>0){pSL = etSL.getText().toString();}
                                else {pSL = "0";}
                                if (etCK.getText().length()>0){pCK = etCK.getText().toString();}
                                else {pCK = "0.0";}
                                if (etGG.getText().length()>0)
                                {pGG = etGG.getText().toString();}
                                else
                                {pGG = "0";}
                                if (etPrice.getText().length()>0) {pPrice = etPrice.getText().toString();}
                                else {pPrice = "0";}

                                float pID = getProductID();
                                if (Integer.parseInt(etSL.getText().toString())<=0){
                                    Toast.makeText(getApplicationContext(),"Vui lòng nhập số lượng dương ",Toast.LENGTH_SHORT).show();
                                }
                                else {
                                    ProductModel productModel = new ProductModel();
                                    productModel.setTenSanPham(pName);
                                    productModel.setSl(Integer.parseInt(pSL));
                                    productModel.setGiaSiCapMot(Integer.parseInt(pPrice));
                                    productModel.setChieuKhau((float) Double.parseDouble(pCK));
                                    productModel.setGiamGia((int) Float.parseFloat(pGG));
                                    productModel.setId((int) pID);
                                    productModels.add(0,productModel);
                                    Log.d("CK1", String.valueOf(productModels.get(0).getChieuKhau()));
                                    Log.d("GG", String.valueOf(productModels.get(0).getGiamGia()));
                                    setStt();
                                    setSumPrice();
                                    tvSumPrice.setText(Utils.ConvertCurrency(String.valueOf(sumPrice)));
                                    tvQuantityTotal.setText(Utils.ConvertCurrency(String.valueOf(quantityTotal(productModels))));
                                    syncPrice();
                                    resetView();
                                    productBill.notifyDataSetChanged();
                                    attv_Product.requestFocus();
                                    attv_Product.setSelectAllOnFocus(true);
                                    attv_Product.selectAll();
                                }
                            }
                        }
                    }
                }
            });
        }

        ////list view billCreate
        productBill = new CustomAdapterProduct(CreateBill.this,R.layout.row_product_createbill,productModels);
        if (lvBillCreate != null) {
            lvBillCreate.setAdapter(productBill);
        }

        if (lvBillCreate != null) {
            lvBillCreate.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, final int position, long id) {


                    AlertDialog.Builder builder = new AlertDialog.Builder(CreateBill.this);
                    builder.setTitle(R.string.option)
                            .setIcon(R.drawable.icon_question)
                            .setItems(new CharSequence[]
                                    { getResources().getString(R.string.update), getResources().getString(R.string.btn_delete)},
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    // The 'which' argument contains the index position
                                    // of the selected item
                                    switch (which) {
                                        case 0:
                                        {
                                            setPosition(position);// to send to dialog edit // using for call back activity
                                            setpName(productModels.get(position).getTenSanPham());
                                            setSl(productModels.get(position).getSl());
                                            setGg(productModels.get(position).getGiamGia());
                                            setDg(productModels.get(position).getGiaSiCapMot());
                                            setCk(productModels.get(position).getChieuKhau());
                                            Log.d("ck",String.valueOf(productModels.get(position).getChieuKhau()));
                                            startDialogEditProduct();
                                            break;
                                        }
                                        case 1:
                                            productModels.remove(position);
                                            setStt();
                                            tvFinalPr.setText(Utils.ConvertCurrency(String.valueOf(setSumPrice())));
                                            tvSumPrice.setText(Utils.ConvertCurrency(String.valueOf(setSumPrice())));
                                            tvQuantityTotal.setText(Utils.ConvertCurrency(String.valueOf(quantityTotal(productModels))));
                                            syncPrice();
                                            productBill.notifyDataSetChanged();
                                            break;
                                    }
                                }
                            });
                    builder.create().show();
                }
            });
        }

        //button save action
        if (btnSave != null)
        {
            btnSave.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (getDataCustomerName().equals("")) {
                        Toast.makeText(getApplicationContext(), "Vui lòng chọn khách hàng !", Toast.LENGTH_SHORT).show();
                    } else {
                        if (productModels.size() == 0) {
                            Toast.makeText(getApplicationContext(), "Vui lòng chọn sản phẩm !", Toast.LENGTH_SHORT).show();
                        } else {
                            Intent intent = new Intent(CreateBill.this, SaveBillSuccess.class);
                            intent.putExtra("data_CustomerName", getDataCustomerName());
                            intent.putExtra("data_CustomerId", getDataCustomerId());
                            intent.putExtra("data_CustomerPhone", getDataCustomerPhone());
                            intent.putExtra("data_CustomerAddress", getDataCustomerAddress());
                            intent.putExtra("data_PriceFinal", tvFinalPr.getText().toString());
                            if (etCKFinal.getText().length()==0){etCKFinal.setText("0.0");}
                            intent.putExtra("CKFinal", etCKFinal.getText().toString());
                            Bundle bundle = new Bundle();
                            bundle.putParcelableArrayList("data_ProductBillDetail", productModels);
                            intent.putExtras(bundle);
                            startActivity(intent);
                        }
                    }
                }
            });
        }
        //set tv option price default
        tvOptionPrice.setText(getResources().getString(R.string.sale_price2));
        price = 2;
        //get data on server
        CheckConnection();
    }
    public void CheckConnection()
    {
        //Check network
        Utils.context=this;
        if(!Utils.isConnectingToInternet()) {
            final android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(this);
            builder.setMessage(getResources().getString(R.string.requiredConnect))
                    .setPositiveButton(getResources().getString(R.string.retry), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            CheckConnection();
                        }
                    })
                    .setNegativeButton(getResources().getString(R.string.close), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            dialogInterface.dismiss();
                        }
                    }).show();
        }
        else
        {
            String CUSTOMER_URL = "http://salemh.skyit.vn/api/KhachHang";
            new CustomerTask().execute(CUSTOMER_URL);
            String PRODUCT_URL = "http://salemh.skyit.vn/api/SanPham";
            new ProductTask().execute(PRODUCT_URL);
        }
    }
    //set position
    public void setPosition(int position){
        this.position = position;
    }

    //get position from list view
    public int getPosition() {
        return position;
    }

    //open dialog edit
    public void startDialogEditProduct(){
        ProductEdit dialogProductEdit = new ProductEdit();
        dialogProductEdit.context = CreateBill.this;
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        getIntent().putExtra("product",productModels.get(position));
        getIntent().putExtra("position",position);
        dialogProductEdit.show(getFragmentManager(),"");
        fragmentTransaction.commit();
    }
    public void updateProduct(){
        for (int i=0; i<productModels.size();i++) {
            if (i==getPosition()){
                productModels.get(i).setSl((int) Float.parseFloat(slEdited));
                productModels.get(i).setGiaSiCapMot(Float.parseFloat(dgEdited));
                productModels.get(i).setChieuKhau(Float.parseFloat(ckEdited));
                productModels.get(i).setGiamGia((int) Float.parseFloat(ggEdited));
            }
        }

    }

    //reset CK and GG
    public void resetView(){
        etCK.setText("0.0");
        etGG.setText("0");
        etCKFinal.setText("0.0");
    }
    public void resetViewOnChangePrice(){
        etCK.setText("0.0");
        etGG.setText("0");
        attv_Product.setText("");
        etPrice.setText("0");
    }
    //dong bo gia tien khi thuc hien hanh dong bat ki
    public void syncPrice(){
        if (etCKFinal.getText().length()>0) {
            float ck = Float.parseFloat(etCKFinal.getText().toString());
            Log.d("ck",String.valueOf(ck));
            if (ck>0) {
                float ip = ck/100;
                int price = (int) (sumPrice - sumPrice*ip);
                tvFinalPr.setText(Utils.ConvertCurrency(String.valueOf(price)));
            }
            else {
                tvFinalPr.setText(Utils.ConvertCurrency((String.valueOf(sumPrice))));
            }
        }
        else {
            tvFinalPr.setText(Utils.ConvertCurrency(String.valueOf(sumPrice)));
        }
    }

    public void removeFragmentSearchCustomer(){
        SearchCustomer searchCustomer = (SearchCustomer)fragmentManager.findFragmentByTag("searchCustomer_Tag");
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        if (searchCustomer!= null) {
            transaction.remove(searchCustomer);
            transaction.commit();
        }
    }
        public void removeFragmentSearchProduct(){
        SearchProduct searchProduct = (SearchProduct) fragmentManager.findFragmentByTag("searchProduct_Tag");
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        if (searchProduct!= null)
        {
            transaction.remove(searchProduct);
            transaction.commit();
        }
    }

    public int quantityTotal(List<ProductModel> pList){
        int sum = 0;
        for (int i = 0;i<pList.size();i++) {
            sum = sum + pList.get(i).getSl();
        }
        return sum;
    }
    @Override
    public void onBackPressed() {
        getFragmentManager().popBackStack();
        super.onBackPressed();
    }


    @Override
    protected void onStop() {
        super.onStop();
        Log.d("Android", "onStop");
    }

    @Override
    protected void onResume() {
        Log.d("CreateBill","onResume");
        attv_Customer.setText(getDataCustomerName());
        attv_Product.setText(getProductName());

        if (!attv_Product.getText().equals("")){
            etSL.setText(String.valueOf(getProductQuantity()));
            etCK.setText(String.valueOf(getProductCK()));
            etCK.setSelectAllOnFocus(true);
//            etGG.setText(String.valueOf(getProductGG));
            etPrice.setText(String.valueOf((int) getProductPrice()));
            etPrice.setSelectAllOnFocus(true);
            etSL.setSelectAllOnFocus(true);
            etSL.selectAll();
        }
        if (attv_Customer.getText().toString().equals(""))
        {
            tvFinalPr.setText(Utils.ConvertCurrency("0"));
            tvSumPrice.setText(Utils.ConvertCurrency("0"));
            etCKFinal.setText("0.0");
            etCKFinal.setSelectAllOnFocus(true);
            tvQuantityTotal.setText("0");
        }
        productBill.notifyDataSetChanged();
        tvSumPrice.setText(Utils.ConvertCurrency(String.valueOf(setSumPrice())));
        tvFinalPr.setText(Utils.ConvertCurrency(String.valueOf(setSumPrice())));
        tvQuantityTotal.setText(Utils.ConvertCurrency(String.valueOf(quantityTotal(productModels))));
        resetView();


        super.onResume();
    }

    @Override
    protected void onDestroy(){
        super.onDestroy();
        Log.d("Android", "onDestroy");
    }

////get data from Search customer activity
    public void setDataCustomer(String cName,String cPhone,String cAdd,String cId){
        this.customerName = cName;
        this.customerPhone = cPhone;
        this.customerAddress = cAdd;
        this.customerId = cId;
    }
    public String getDataCustomerName(){
        return customerName;
    }
    public String getDataCustomerPhone(){
        return customerPhone;
    }
    public String getDataCustomerAddress() {
        return customerAddress;
    }
    public String getDataCustomerId() {
        return customerId;
    }

    public void setDataProduct(String pName,float pPrice,int pQuantity,float pCK,int pID,String pCode){
        this.productName = pName;
        this.productPrice = pPrice;
        this.productQuantity = pQuantity;
        this.productCK = pCK;
        this.productID = pID;
        this.productCode = pCode;
    }
    public String getProductName() {
        return productName;
    }
    public int getProductQuantity() {
        return productQuantity;
    }
    public float getProductCK() {
        return productCK;
    }
    public float getProductGG() {
        return productGG;
    }
    public float getProductPrice() {
        return productPrice;
    }
    public float getProductID() {
        return productID;
    }
    public String getProductCode() {
        return productCode;
    }

//    @Override
//    public void onFinishEditDialog(String a,String b) {
//        Toast.makeText(getApplicationContext(), "Hi"+ a + b,Toast.LENGTH_LONG).show();
//    }
    @Override
    public void onFinishEditDialog(ProductModel productModel, String position) {
        this.slEdited = String.valueOf(productModel.getSl());
        this.dgEdited = String.valueOf(productModel.getGiaSiCapMot());
        this.ckEdited = String.valueOf(productModel.getChieuKhau());
        this.ggEdited = String.valueOf(productModel.getGiamGia());
    }

    private class CustomerTask extends AsyncTask<String, String, List<CustomerModel> >{


        @Override
        protected List<CustomerModel> doInBackground(String... params) {
            HttpURLConnection connection = null;
            BufferedReader reader = null;
            try {
                URL url = new URL(params[0]);
                connection = (HttpURLConnection)url.openConnection();
                connection.connect();
                InputStream inputStream = connection.getInputStream();
                reader = new BufferedReader(new InputStreamReader(inputStream));

                StringBuffer buffer = new StringBuffer();

                String line;
                while ((line = reader.readLine())!=null){
                    buffer.append(line);
                }
                String jsonFinal = buffer.toString();
                //Log.d("JSONNNNNNN",jsonFinal);
                JSONArray parentArray  =  new JSONArray(jsonFinal);
                List<CustomerModel> customerModelList = new ArrayList<>();
                for(int i = 0;i< parentArray.length();i++)
                {
                    JSONObject objectFinal = parentArray.getJSONObject(i);
                    try {
                        CustomerModel customerModel= new CustomerModel();
                        customerModel.setId(objectFinal.getInt("id"));
                        customerModel.setMaKH(objectFinal.getString("maKhachHang"));
                        customerModel.setTenKH(objectFinal.getString("tenKhachHang"));
                        customerModel.setDiaChi(objectFinal.getString("diaChi"));
                        customerModel.setSdt(objectFinal.getString("soDienThoai"));
                        customerModel.setEmail(objectFinal.getString("email"));
                        customerModel.setCongNo(objectFinal.getInt("congNoDauKy"));
                        customerModel.setSoFax(objectFinal.getString("soFAX"));
                        customerModel.setMsThue(objectFinal.getString("maSoThue"));
                        customerModel.setNgayNhap(objectFinal.getString("ngayNhap"));
                        customerModel.setCongNoMax(objectFinal.getInt("congNoMax"));
                        customerModel.setTenString(objectFinal.getString("tenKhachHangString"));
                        customerModel.setHanTra(objectFinal.getString("hanTraNo"));
                        customerModelList.add(customerModel);

                    } catch (JSONException ignored) {
                    }
                }
                //Log.d("result", customerModelList.get(1).getTenKH());

                return customerModelList;

            } catch (IOException | JSONException e) {
                e.printStackTrace();
            } finally {
                if (connection!=null){
                    connection.disconnect();
                }
                try {
                   if (reader!=null){
                       reader.close();
                   }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(List<CustomerModel> result) {
            super.onPostExecute(result);
            searchCustomerModels = result;
            if (result!= null){
                //Log.d("result", result.get(1).getTenKH());
                adapter_Customer = new CustomAdapterCustomer(CreateBill.this,R.layout.row_customer,result);
                attv_Customer.setAdapter(adapter_Customer);
                attv_Customer.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                        setDataCustomer(adapter_Customer.customerModelList.get(position).getTenKH(), adapter_Customer.customerModelList.get(position).getSdt(),
                                adapter_Customer.customerModelList.get(position).getDiaChi(), String.valueOf(adapter_Customer.customerModelList.get(position).getId()));
                        attv_Product.setEnabled(true);
                        attv_Product.requestFocus();
                    }
                });
            }
            else {
                AlertDialog.Builder builder = new AlertDialog.Builder(CreateBill.this);
                builder.setMessage(R.string.empty_data)
                        .setPositiveButton(R.string.close, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                                finish();
                            }
                        })
                        .show();
            }

        }
    }

    public class ProductTask extends AsyncTask<String, String, List<ProductModel> >{
        private ProgressDialog dialog = new ProgressDialog(CreateBill.this);
        @Override
        protected void onPreExecute() {
            dialog.setMessage(getResources().getString(R.string.please_wait));
            dialog.setButton(DialogInterface.BUTTON_NEGATIVE, getResources().getString(R.string.close), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });
            dialog.show();
        }
        @Override
        protected List<ProductModel> doInBackground(String... params) {

            HttpURLConnection connection = null;
            BufferedReader reader = null;

            try {
                URL url = new URL(params[0]);
                connection = (HttpURLConnection)url.openConnection();
                connection.connect();
                InputStream inputStream = connection.getInputStream();
                reader = new BufferedReader(new InputStreamReader(inputStream));
                StringBuffer buffer = new StringBuffer();
                String line;
                while ((line = reader.readLine())!=null){
                    buffer.append(line);
                }
                String jsonFinal = buffer.toString();
                JSONArray parentArray  =  new JSONArray(jsonFinal);
                List<ProductModel> productModelList = new ArrayList<>();
                for(int i = 0;i< parentArray.length();i++)
                {
                    JSONObject objectFinal = parentArray.getJSONObject(i);
                    try {
                        ProductModel productModel= new ProductModel();
                        productModel.setId(objectFinal.getInt("id"));
                        productModel.setMaSanPham(objectFinal.getString("maSanPham"));
                        productModel.setTenSanPham(objectFinal.getString("tenSanPham"));
                        productModel.setDonVi(objectFinal.getString("donVi"));
                        productModel.setSoLuong((float) objectFinal.getDouble("soLuong"));
                        productModel.setIdLoaiSanPham(objectFinal.getInt("idLoaiSanPham"));
                        productModel.setMoTa(objectFinal.getString("moTa"));
                        productModel.setNgayTao(objectFinal.getString("ngayTao"));
                        productModel.setGiaBan((float) objectFinal.getDouble("giaBan"));
                        productModel.setGiaSiCapMot((float) objectFinal.getDouble("giaSiCapMot"));
                        productModel.setGiaSiCapHai((float) objectFinal.getDouble("giaSiCapHai"));

                        productModelList.add(productModel);
                    }
                    catch (JSONException e)
                    {
                        continue;
                    }
                }
                return productModelList;

            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }finally {
                if (connection!=null){
                    connection.disconnect();
                }
                try {
                    if (reader!=null){
                        reader.close();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(List<ProductModel> result) {
            super.onPostExecute(result);
            searchProductModels = result;
            if (result!= null){
                adapter_Product=new customadapter(CreateBill.this,R.layout.row_product,result);
                attv_Product.setAdapter(adapter_Product);
                attv_Product.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                        etPrice.setText(String.valueOf((int) adapter_Product.productModelList.get(position).getGiaSiCapMot()));
                        if (price == 1){
                            setDataProduct(adapter_Product.productModelList.get(position).getTenSanPham(),adapter_Product.productModelList.get(position).getGiaBan()
                                    ,adapter_Product.productModelList.get(position).getSl(),adapter_Product.productModelList.get(position).getChieuKhau(),
                                    adapter_Product.productModelList.get(position).getId(),adapter_Product.productModelList.get(position).getMaSanPham() );
                            etPrice.setText(String.valueOf((int) adapter_Product.productModelList.get(position).getGiaBan()));
                        }
                        if (price == 2){
                            setDataProduct(adapter_Product.productModelList.get(position).getTenSanPham(),adapter_Product.productModelList.get(position).getGiaSiCapMot()
                                    ,adapter_Product.productModelList.get(position).getSl(),adapter_Product.productModelList.get(position).getChieuKhau(),
                                    adapter_Product.productModelList.get(position).getId(),adapter_Product.productModelList.get(position).getMaSanPham() );
                            etPrice.setText(String.valueOf((int) adapter_Product.productModelList.get(position).getGiaSiCapMot()));
                        }
                        if (price == 3){
                            setDataProduct(adapter_Product.productModelList.get(position).getTenSanPham(),adapter_Product.productModelList.get(position).getGiaSiCapHai()
                                    ,adapter_Product.productModelList.get(position).getSl(),adapter_Product.productModelList.get(position).getChieuKhau(),
                                    adapter_Product.productModelList.get(position).getId(),adapter_Product.productModelList.get(position).getMaSanPham() );
                            etPrice.setText(String.valueOf((int) adapter_Product.productModelList.get(position).getGiaSiCapHai()));
                        }
                        etSL.requestFocus();
                    }
                });
            }
            else {
                AlertDialog.Builder builder = new AlertDialog.Builder(CreateBill.this);
                builder.setMessage(R.string.empty_data)
                        .setPositiveButton(R.string.close, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                                finish();
                            }
                        })
                        .show();

            }
            if (dialog.isShowing()){
                dialog.dismiss();
            }
        }
    }

    //set stt
    public static void setStt() {
        int stt = 1;
        for (int i = 0;i<productModels.size();i++)
        {
            productModels.get(i).setStt(stt);
            stt++;
        }
    }
    ////cal money
    public static int setSumPrice(){
        int gg;
        if (etGG.getText().length()>0){
             gg = Integer.parseInt(etGG.getText().toString());
        }
        else
        {
            etGG.setText("0");
            gg = 0;
        }
        if (productModels!= null)
        {
            sumPrice = 0;
            for (ProductModel productModel: productModels )
            {
                sumPrice = (int) (sumPrice +((productModel.getSl() * productModel.getGiaSiCapMot())-((productModel.getSl() * productModel.getGiaSiCapMot())*productModel.getChieuKhau())/100 -
                productModel.getSl()*gg));
            }
        }
        return sumPrice;
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_create_bill, menu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == android.R.id.home) {
            this.finish();
            return true;
        }
        if (id == R.id.action_choose_price1)
        {
            if (price!=1){

                if (productModels.size()>0){
                    AlertDialog.Builder builder = new AlertDialog.Builder(this);
                    builder.setTitle("Thay đổi khung giá")
                            .setIcon(R.drawable.icon_question)
                            .setMessage("Sản phẩm đã chọn sẽ bị xóa! Tiếp tục?")
                            .setPositiveButton("Có", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    price = 1;
                                    optionPrice = getResources().getString(R.string.sale_price1);
                                    tvOptionPrice.setText(optionPrice);
                                    adapter_Product.notifyDataSetChanged();
                                    productModels.clear();
                                    productBill.notifyDataSetChanged();
                                    onResume();
                                    resetViewOnChangePrice();
                                }
                            })
                            .setNegativeButton("Không", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            })
                            .show();
                }
                else {
                    price = 1;
                    optionPrice = getResources().getString(R.string.sale_price1);
                    tvOptionPrice.setText(optionPrice);
                    adapter_Product.notifyDataSetChanged();
                }
            }
        }
        if (id == R.id.action_choose_price2)
        {
            if (price!=2){

                if (productModels.size()>0){
                    AlertDialog.Builder builder = new AlertDialog.Builder(this);
                    builder.setTitle("Thay đổi khung giá")
                            .setIcon(R.drawable.icon_question)
                            .setMessage("Sản phẩm đã chọn sẽ bị xóa! Tiếp tục?")
                            .setPositiveButton("Có", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    price = 2;
                                    adapter_Product.notifyDataSetChanged();
                                    optionPrice = getResources().getString(R.string.sale_price2);
                                    tvOptionPrice.setText(optionPrice);
                                    productModels.clear();
                                    productBill.notifyDataSetChanged();// reload list_view once clear
                                    onResume();
                                    resetViewOnChangePrice();
                                }
                            })
                            .setNegativeButton("Không", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            })
                            .show();
                }
                else {
                    price = 2;
                    adapter_Product.notifyDataSetChanged();
                    optionPrice = getResources().getString(R.string.sale_price2);
                    tvOptionPrice.setText(optionPrice);
                }
            }
        }
        if (id == R.id.action_choose_price3)
        {
            if (price!=3){

                if (productModels.size()>0){
                    AlertDialog.Builder builder = new AlertDialog.Builder(this);
                    builder.setTitle("Thay đổi khung giá")
                            .setIcon(R.drawable.icon_question)
                            .setMessage("Sản phẩm đã chọn sẽ bị xóa! Tiếp tục?")
                            .setPositiveButton("Có", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    price = 3;
                                    optionPrice = getResources().getString(R.string.sale_price3);
                                    tvOptionPrice.setText(optionPrice);
                                    adapter_Product.notifyDataSetChanged();
                                    productModels.clear();
                                    productBill.notifyDataSetChanged();
                                    onResume();
                                    resetViewOnChangePrice();
                                }
                            })
                            .setNegativeButton("Không", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            })
                            .show();
                }
                else {
                    price = 3;
                    optionPrice = getResources().getString(R.string.sale_price3);
                    tvOptionPrice.setText(optionPrice);
                    adapter_Product.notifyDataSetChanged();
                }
            }
        }
        return super.onOptionsItemSelected(item);
    }
}

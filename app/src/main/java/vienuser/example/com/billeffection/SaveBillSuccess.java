package vienuser.example.com.billeffection;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

import vienuser.example.com.billeffection.models.ProductModel;
import vienuser.example.com.billeffection.models.Utils;

public class SaveBillSuccess extends AppCompatActivity {

    private ListView lvItemSave;
    public static ArrayList<ProductModel> productModels_Saved;
    private CustomAdapterProduct savedAdapterProduct;
    private TextView tvSumPriceFinal;
    private TextView tvCustomerName;
    private TextView tvCustomerPhone;
    private TextView tvCustomerAddress;
    private TextView tvCustomerId;
    private TextView tvDate;
    private EditText etNote;
    private TextView tvCKFinal;
    private EditText etCustomerPay;
    JSONObject paramsBill;
    JSONArray paramsBillDetail;
    JSONObject objectBillDetail;
    JSONArray paramsThuNo;
    SaveBillSuccess context;

    String billURL =  "http://salemh.skyit.vn/api/HoaDon/";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_save_bill_success);
        this.setTitle(Html.fromHtml("<font color='#131313'>Chi tiết hóa đơn</font>"));
//        this.setTitle(getResources().getString(R.string.billDetail));
        Toolbar toolbar = (Toolbar) findViewById(R.id.app_bar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        tvSumPriceFinal = (TextView)findViewById(R.id.tv_SumPriceFinal);
        tvCustomerName = (TextView)findViewById(R.id.tv_CustomerName);
        tvCustomerPhone = (TextView)findViewById(R.id.tv_CustomerPhone);
        tvCustomerAddress = (TextView)findViewById(R.id.tv_CustomerAddress);
        lvItemSave = (ListView)findViewById(R.id.lv_ItemSave);
        tvDate = (TextView)findViewById(R.id.tv_DataCreate) ;
        etNote = (EditText)findViewById(R.id.et_Note);
        tvCKFinal = (TextView)findViewById(R.id.tv_CKFinal);
        etCustomerPay = (EditText)findViewById(R.id.et_CustomerPay);

        Button btnSaveOnDataBase = (Button)findViewById(R.id.btn_SaveFinal);

        String date = new SimpleDateFormat("dd/MM/yyyy").format(new Date());

        Intent i = getIntent();
        //customer
        String customerName = i.getStringExtra("data_CustomerName");
        final String customerId = i.getStringExtra("data_CustomerId");
        String phone = i.getStringExtra("data_CustomerPhone");
        String address = i.getStringExtra("data_CustomerAddress");
        String priceFinal = i.getStringExtra("data_PriceFinal");
        String ckFinal = i.getStringExtra("CKFinal");

        Bundle bundle = getIntent().getExtras();
        productModels_Saved = bundle.getParcelableArrayList("data_ProductBillDetail");

        savedAdapterProduct = new CustomAdapterProduct(SaveBillSuccess.this,R.layout.row_item_save,productModels_Saved);
        Log.d("CK",String.valueOf(productModels_Saved.get(0).getChieuKhau()));
        Log.d("GG",String.valueOf(productModels_Saved.get(0).getGiamGia()));
        Log.d("idsp",String.valueOf(productModels_Saved.get(0).getId()));

        lvItemSave.setAdapter(savedAdapterProduct);
        //set
        tvSumPriceFinal.setText(priceFinal);
        tvCustomerName.setText(customerName);
        tvCustomerPhone.setText(phone);
        tvCustomerAddress.setText(address);
        tvDate.setText(date);
        tvCKFinal.setText(ckFinal);

        //Save On Database
        if (btnSaveOnDataBase != null) {
            btnSaveOnDataBase.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(SaveBillSuccess.this);
                    builder.setTitle(R.string.confirm_save);
                    builder.setIcon(R.drawable.icon_question);
                    builder.setPositiveButton("Không", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.dismiss();
                            }
                        })

//                    .setNeutralButton("Lưu và In", new DialogInterface.OnClickListener() {
//                            public void onClick(DialogInterface dialog, int id) {
//                                paramsBill = new JSONObject();
//                                paramsBillDetail = new JSONArray();
//                                for (ProductModel productModel:productModels_Saved)
//                                {
//                                    objectBillDetail = new JSONObject();
//                                    try {
//                                        objectBillDetail.put("idSanPham",String.valueOf(productModel.getId()));
//                                        objectBillDetail.put("soLuong",productModel.getSl());
//                                        objectBillDetail.put("donGia",productModel.getGiaSiCapMot());
//                                        objectBillDetail.put("chietKhau",productModel.getChieuKhau());
//                                        objectBillDetail.put("giamGia",productModel.getGiamGia());
//                                    } catch (JSONException e) {
//                                        e.printStackTrace();
//                                    }
//                                    paramsBillDetail.put(objectBillDetail);
//                                }
//
//                                try {
//
//                                    paramsBill.put("idNhanVien",Integer.parseInt(LoginSuccess.strId));
//                                    paramsBill.put("idKhachHang",customerId);
//                                    paramsBill.put("chietKhau",tvCKFinal.getText());
//                                    paramsBill.put("VAT",0);
//                                    paramsBill.put("ghiChu",etNote.getText().toString());
//                                    paramsBill.put("soTienKhachDua",etCustomerPay.getText().toString());
//                                    paramsBill.put("loaiHoaDon",1);
//                                    paramsBill.put("hinhThucThanhToan","TM");
//                                    paramsBill.put("tbChiTietHoaDons",paramsBillDetail);
//
//                                } catch (JSONException e) {
//                                    e.printStackTrace();
//                                }
//                                CheckConnection();
//                            }
//                    })

                    .setNegativeButton("Lưu",new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                            paramsBill = new JSONObject();
                            paramsBillDetail = new JSONArray();
                            paramsThuNo =new JSONArray();
                            JSONObject paramsThuNoDetail = new JSONObject();
                            try {
                                paramsThuNoDetail.put("idNhanVien",Integer.parseInt(LoginSuccess.strId));
                                paramsThuNoDetail.put("idKhachHang",customerId);
                                paramsThuNoDetail.put("GhiChu","Thanh toán tiền hàng");
                                if(Integer.parseInt(tvSumPriceFinal.getText().toString().replace(",",""))<=Integer.parseInt(etCustomerPay.getText().toString()))
                                    paramsThuNoDetail.put("SoTien",tvSumPriceFinal.getText().toString().replace(",",""));
                                else
                                    paramsThuNoDetail.put("SoTien",etCustomerPay.getText().toString());
                                paramsThuNo.put(paramsThuNoDetail);
                            } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            for (ProductModel productModel:productModels_Saved)
                            {
                                objectBillDetail = new JSONObject();
                                try {
                                    objectBillDetail.put("idSanPham",String.valueOf(productModel.getId()));
                                    objectBillDetail.put("soLuong",productModel.getSl());
                                    objectBillDetail.put("donGia",productModel.getGiaSiCapMot());
                                    objectBillDetail.put("chietKhau",productModel.getChieuKhau());
                                    objectBillDetail.put("giamGia",productModel.getGiamGia());
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                                paramsBillDetail.put(objectBillDetail);
                            }

                            try {

                                paramsBill.put("idNhanVien",Integer.parseInt(LoginSuccess.strId));
                                paramsBill.put("idKhachHang",customerId);
                                paramsBill.put("chietKhau",tvCKFinal.getText());
                                paramsBill.put("VAT",0);
                                paramsBill.put("ghiChu",etNote.getText().toString());
                                paramsBill.put("soTienKhachDua",etCustomerPay.getText().toString().replace(",",""));
                                paramsBill.put("loaiHoaDon",1);
                                paramsBill.put("hinhThucThanhToan","TM");
                                paramsBill.put("tbChiTietHoaDons",paramsBillDetail);
                                paramsBill.put("tbThuNoKhachHangs",paramsThuNo);

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            CheckConnection();
                        }
                    })
                    .show();

                }
            });
        }
    }
    //Check internet connection
    public void CheckConnection()
    {
        //Check network
        Utils.context=this;
        if(!Utils.isConnectingToInternet()) {
            final AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setMessage(getResources().getString(R.string.requiredConnect))
                    .setPositiveButton(getResources().getString(R.string.retry), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            CheckConnection();
                        }
                    })
                    .setNegativeButton(getResources().getString(R.string.close), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            dialogInterface.dismiss();
                        }
                    }).show();
        }
        else
        {
            String jsonEntity = new String(paramsBill.toString().getBytes());
            RequestItemsServiceTask task = new RequestItemsServiceTask();
            task.requestItemsServiceTask(jsonEntity,billURL);
            task.execute();

        }
    }

    private class RequestItemsServiceTask extends AsyncTask<Void, Void, Void> {
        String result="";
        String jsonEntity="";
        String url;
        private ProgressDialog dialog = new ProgressDialog(SaveBillSuccess.this);

        public void requestItemsServiceTask(String jsonEntity, String url)
        {
            this.jsonEntity=jsonEntity;
            Log.d("JSON:",jsonEntity);
            this.url = url;
        }
        @Override
        protected void onPreExecute() {
            // TODO i18n
            dialog.setMessage(getResources().getString(R.string.please_wait));
            dialog.setCancelable(false);
            dialog.setCanceledOnTouchOutside(false);
            dialog.setButton(DialogInterface.BUTTON_NEGATIVE, getResources().getString(R.string.close), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                    cancel(true);
                }
            });
            dialog.show();
        }

        @Override
        protected Void doInBackground(Void... unused) {
            try {
                String surl = url;
                result= Utils.makeRequest2("POST",surl,null,"application/json", jsonEntity);

            } catch (UnsupportedEncodingException e) {
                //e.printStackTrace();
            } catch (ProtocolException e) {
                //e.printStackTrace();
            } catch (MalformedURLException e) {
                // e.printStackTrace();
            } catch (IOException e) {
                //e.printStackTrace();
            }

            return null;
        }
        @Override
        protected void onPostExecute(Void unused) {
            if(result.equals("true")) {
                Toast.makeText(getApplicationContext(), "Lưu thành công! ", Toast.LENGTH_LONG).show();
                if (dialog.isShowing()){
                    dialog.dismiss();
                }
                CreateBill.productModels.clear();
                finish();
            }
            else  if(result.equals("false")) {
                Toast.makeText(getApplicationContext(),"Lưu không thành công!", Toast.LENGTH_LONG).show();

            }
            else {
                dialog.setMessage("Không thể kết nối đến server");
            }
        }
    }

    public static String formateDate(String dateString) {
        Date date;
        String formattedDate = "";
        try {
            date = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.getDefault()).parse(dateString);
            formattedDate = new SimpleDateFormat("dd/MM/yyyy",Locale.getDefault()).format(date);
        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return formattedDate;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == android.R.id.home) {
            this.finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}

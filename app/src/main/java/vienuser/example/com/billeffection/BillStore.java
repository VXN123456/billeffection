package vienuser.example.com.billeffection;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.SearchView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Locale;

import vienuser.example.com.billeffection.models.BillModel;
import vienuser.example.com.billeffection.models.Utils;

public class BillStore extends AppCompatActivity {
    SearchView sv;
    ListView lv;
    private ArrayList<BillModel> itemsList;
    private ArrayList<String> usernameList;
    private ArrayList<String> customerList;
    CustomAdapter_Bill billAdapter;
    Button bt_cancel;
    Button bt_filter;
    LinearLayout layout;
    ImageButton pickfrom;
    ImageButton pickto;
    AutoCompleteTextView act_username;
    AutoCompleteTextView act_customername;
    public EditText et_fromdate;
    public EditText et_todate;
    private int mYear, mMonth, mDay,mYear2, mMonth2, mDay2;
    String myFormat = "dd/MM/yyyy";
    final SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        //You can now use and reference the ActionBar
        this.setTitle(Html.fromHtml("<font color='#131313'>Thống kê hóa đơn</font>"));
//        this.setTitle(getResources().getString(R.string.tv_ListBill));
        setContentView(R.layout.activity_bill_store);
        Toolbar toolbar = (Toolbar) findViewById(R.id.app_bar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        layout = (LinearLayout)findViewById(R.id.layout_filter);
        sv = (SearchView)findViewById(R.id.sv_bill);
        lv = (ListView)findViewById(R.id.lv_bill);
        pickfrom = (ImageButton)findViewById(R.id.ibt_pickfrom);
        pickto=(ImageButton)findViewById(R.id.ibt_pickto);
        et_fromdate=(EditText)findViewById(R.id.et_fromdate);
        et_todate=(EditText)findViewById(R.id.et_todate);
        bt_filter=(Button)findViewById(R.id.bt_filter);
        act_customername=(AutoCompleteTextView)findViewById(R.id.act_customername);
        act_username=(AutoCompleteTextView)findViewById(R.id.act_username);
        usernameList = new ArrayList<>();
        customerList = new ArrayList<>();
        bt_filter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.d("ten nhan vien",itemsList.get(0).getTenNhanVien());
                ArrayList<BillModel> results = new ArrayList<>();
                // filter for all fields are empty
                if(act_customername.getText().toString().length()==0 && act_username.getText().toString().length()==0
                        && et_fromdate.getText().toString().length()==0&& et_todate.getText().toString().length()==0)
                {
                    results=itemsList;// list after filter
                    billAdapter.orig=results;//list origin
                    billAdapter.notifyDataSetChanged();// update adapter after filter
                }

                //filter only field is not empty
                if(et_fromdate.getText().toString().equals("") && act_customername.getText().toString().equals("")
                        && !act_username.getText().toString().equals("")&& et_todate.getText().toString().equals("")) {
                    for (final BillModel g : itemsList) {
                        if (Utils.ConvertString(g.getTenNhanVien().toLowerCase()).contains(Utils.ConvertString(act_username.getText().toString()))) {
                            results.add(g);
                        }
                    }
                }
                // only customer
                if(et_fromdate.getText().toString().equals("") && !act_customername.getText().toString().equals("")
                        && act_username.getText().toString().equals("")&& et_todate.getText().toString().equals("")) {
                    for (final BillModel g : itemsList) {
                        if (Utils.ConvertString(g.getTenKhachHang().toLowerCase()).contains(Utils.ConvertString(act_customername.getText().toString()))) {
                            results.add(g);
                        }
                    }
                }
                //username and customer
                if(et_fromdate.getText().toString().equals("") && !act_customername.getText().toString().equals("")
                        && !act_username.getText().toString().equals("")&& et_todate.getText().toString().equals("")) {
                    for (final BillModel g : itemsList) {
                        if (Utils.ConvertString(g.getTenKhachHang().toLowerCase()).contains(Utils.ConvertString(act_customername.getText().toString()))
                                && Utils.ConvertString(g.getTenNhanVien().toLowerCase()).contains(Utils.ConvertString(act_username.getText().toString()))) {
                            results.add(g);
                        }
                    }
                }
                //only from day
                if(!et_fromdate.getText().toString().equals("") && act_customername.getText().toString().equals("")
                        && act_username.getText().toString().equals("")&& et_todate.getText().toString().equals("")) {
                    for (final BillModel g : itemsList) {
                        try {
                            if ((sdf.parse(Utils.ChangeDateFormat(g.getNgayLap())).compareTo(sdf.parse(et_fromdate.getText().toString())) >= 0)) {
                                results.add(g);
                            }
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }
                    }
                }
                //only from day and user
                if(!et_fromdate.getText().toString().equals("") && act_customername.getText().toString().equals("")
                        && !act_username.getText().toString().equals("")&& et_todate.getText().toString().equals("")) {
                    for (final BillModel g : itemsList) {
                        try {
                            if ((sdf.parse(Utils.ChangeDateFormat(g.getNgayLap())).compareTo(sdf.parse(et_fromdate.getText().toString())) >= 0)
                                    && Utils.ConvertString(g.getTenNhanVien().toLowerCase()).contains(Utils.ConvertString(act_username.getText().toString()))) {
                                results.add(g);
                            }
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }
                    }
                }
                //only from day and customer
                if(!et_fromdate.getText().toString().equals("") && !act_customername.getText().toString().equals("")
                        && act_username.getText().toString().equals("")&& et_todate.getText().toString().equals("")) {
                    for (final BillModel g : itemsList) {
                        try {
                            if ((sdf.parse(Utils.ChangeDateFormat(g.getNgayLap())).compareTo(sdf.parse(et_fromdate.getText().toString())) >= 0)
                                    && Utils.ConvertString(g.getTenKhachHang().toLowerCase()).contains(Utils.ConvertString(act_customername.getText().toString()))) {
                                results.add(g);
                            }
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }
                    }
                }
                //only to date and user
                if(et_fromdate.getText().toString().equals("") && act_customername.getText().toString().equals("")
                        && !act_username.getText().toString().equals("")&& !et_todate.getText().toString().equals("")) {
                    for (final BillModel g : itemsList) {
                        try {
                            if ((sdf.parse(Utils.ChangeDateFormat(g.getNgayLap())).compareTo(sdf.parse(et_todate.getText().toString())) <= 0)
                                    && Utils.ConvertString(g.getTenNhanVien().toLowerCase()).contains(Utils.ConvertString(act_username.getText().toString()))) {
                                results.add(g);
                            }
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }
                    }
                }
                //only to date and customer
                if(et_fromdate.getText().toString().equals("") && !act_customername.getText().toString().equals("")
                        && act_username.getText().toString().equals("")&& !et_todate.getText().toString().equals("")) {
                    for (final BillModel g : itemsList) {
                        try {
                            if ((sdf.parse(Utils.ChangeDateFormat(g.getNgayLap())).compareTo(sdf.parse(et_todate.getText().toString())) <= 0)
                                    && Utils.ConvertString(g.getTenKhachHang().toLowerCase()).contains(Utils.ConvertString(act_customername.getText().toString()))) {
                                results.add(g);
                            }
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }
                    }
                }
                //only to date
                if(et_fromdate.getText().toString().equals("") && act_customername.getText().toString().equals("")
                        && act_username.getText().toString().equals("")&& !et_todate.getText().toString().equals("")) {
                    for (final BillModel g : itemsList) {
                        try {
                            if ((sdf.parse(Utils.ChangeDateFormat(g.getNgayLap())).compareTo(sdf.parse(et_todate.getText().toString())) <= 0)) {
                                results.add(g);
                            }
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }
                    }
                }
                //from date and to day
                if(!et_fromdate.getText().toString().equals("") && act_customername.getText().toString().equals("")
                        && act_username.getText().toString().equals("")&& !et_todate.getText().toString().equals("")) {
                    for (final BillModel g : itemsList) {
                        try {
                            if ((sdf.parse(Utils.ChangeDateFormat(g.getNgayLap())).compareTo(sdf.parse(et_fromdate.getText().toString())) >=0)
                                    && (sdf.parse(Utils.ChangeDateFormat(g.getNgayLap())).compareTo(sdf.parse(et_todate.getText().toString())) <= 0)) {
                                results.add(g);
                            }
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }
                    }
                }
                // username and date
                if(!et_fromdate.getText().toString().equals("") && act_customername.getText().toString().equals("")
                        && !act_username.getText().toString().equals("")&& !et_todate.getText().toString().equals("")) {
                    for (final BillModel g : itemsList) {
                        try {
                            if ((sdf.parse(Utils.ChangeDateFormat(g.getNgayLap())).compareTo(sdf.parse(et_fromdate.getText().toString())) >=0)
                                    && (sdf.parse(Utils.ChangeDateFormat(g.getNgayLap())).compareTo(sdf.parse(et_todate.getText().toString())) <= 0)
                                    && Utils.ConvertString(g.getTenNhanVien().toLowerCase()).contains(Utils.ConvertString(act_username.getText().toString()))) {
                                results.add(g);
                            }
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }
                    }
                }
                // customer and date
                if(!et_fromdate.getText().toString().equals("") && !act_customername.getText().toString().equals("")
                        && act_username.getText().toString().equals("")&& !et_todate.getText().toString().equals("")) {
                    for (final BillModel g : itemsList) {
                        try {
                            if ((sdf.parse(Utils.ChangeDateFormat(g.getNgayLap())).compareTo(sdf.parse(et_fromdate.getText().toString())) >=0)
                                    && (sdf.parse(Utils.ChangeDateFormat(g.getNgayLap())).compareTo(sdf.parse(et_todate.getText().toString())) <= 0)
                                    && Utils.ConvertString(g.getTenKhachHang().toLowerCase()).contains(Utils.ConvertString(act_customername.getText().toString()))) {
                                results.add(g);
                            }
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }
                    }
                }
                // username,customer and from date
                if(!et_fromdate.getText().toString().equals("") && !act_customername.getText().toString().equals("")
                        && !act_username.getText().toString().equals("")&& et_todate.getText().toString().equals("")) {
                    for (final BillModel g : itemsList) {
                        try {
                            if ((sdf.parse(Utils.ChangeDateFormat(g.getNgayLap())).compareTo(sdf.parse(et_fromdate.getText().toString())) >=0)
                                    && Utils.ConvertString(g.getTenNhanVien().toLowerCase()).contains(Utils.ConvertString(act_username.getText().toString()))
                                    && Utils.ConvertString(g.getTenKhachHang().toLowerCase()).contains(Utils.ConvertString(act_customername.getText().toString()))) {
                                results.add(g);
                            }
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }
                    }
                }
                // username,customer and to date
                if(et_fromdate.getText().toString().equals("") && !act_customername.getText().toString().equals("")
                        && !act_username.getText().toString().equals("")&& !et_todate.getText().toString().equals("")) {
                    for (final BillModel g : itemsList) {
                        try {
                            if ((sdf.parse(Utils.ChangeDateFormat(g.getNgayLap())).compareTo(sdf.parse(et_todate.getText().toString())) <= 0)
                                    && Utils.ConvertString(g.getTenNhanVien().toLowerCase()).contains(Utils.ConvertString(act_username.getText().toString()))
                                    && Utils.ConvertString(g.getTenKhachHang().toLowerCase()).contains(Utils.ConvertString(act_customername.getText().toString()))) {
                                results.add(g);
                            }
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }
                    }
                }
                // username,customer and date
                if(!et_fromdate.getText().toString().equals("") && !act_customername.getText().toString().equals("")
                        && !act_username.getText().toString().equals("")&& !et_todate.getText().toString().equals("")) {
                    for (final BillModel g : itemsList) {
                        try {
                            if ((sdf.parse(Utils.ChangeDateFormat(g.getNgayLap())).compareTo(sdf.parse(et_fromdate.getText().toString())) >=0)
                                    && (sdf.parse(Utils.ChangeDateFormat(g.getNgayLap())).compareTo(sdf.parse(et_todate.getText().toString())) <= 0)
                                    && Utils.ConvertString(g.getTenNhanVien().toLowerCase()).contains(Utils.ConvertString(act_username.getText().toString()))
                                    && Utils.ConvertString(g.getTenKhachHang().toLowerCase()).contains(Utils.ConvertString(act_customername.getText().toString()))) {
                                results.add(g);
                            }
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }
                    }
                }

                billAdapter.lbill = results;
                billAdapter.orig = results;
                billAdapter.notifyDataSetChanged();

            }
        });
        final Calendar c = Calendar.getInstance();
        mYear = c.get(Calendar.YEAR);
        mMonth = c.get(Calendar.MONTH);
        mDay = c.get(Calendar.DAY_OF_MONTH);
        final Calendar c2 = Calendar.getInstance();
        mYear2 = c2.get(Calendar.YEAR);
        mMonth2 = c2.get(Calendar.MONTH);
        mDay2 = c2.get(Calendar.DAY_OF_MONTH);
        pickfrom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                DatePickerDialog dpd = new DatePickerDialog(BillStore.this, new DatePickerDialog.OnDateSetListener() {
                            @Override
                            public void onDateSet(DatePicker view, int year,
                                                  int monthOfYear, int dayOfMonth) {
                                final Calendar ca = Calendar.getInstance();
                                ca.set(Calendar.YEAR,year);
                                ca.set(Calendar.MONTH,monthOfYear);
                                ca.set(Calendar.DAY_OF_MONTH,dayOfMonth);
                                mYear=year;
                                mMonth=monthOfYear;
                                mDay=dayOfMonth;
                                et_fromdate.setText(sdf.format(ca.getTime()));
                            }
                        }, mYear, mMonth, mDay);
                dpd.show();
            }
        });
        pickto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DatePickerDialog dpd = new DatePickerDialog(BillStore.this, new DatePickerDialog.OnDateSetListener() {

                            @Override
                            public void onDateSet(DatePicker view, int year,
                                                  int monthOfYear, int dayOfMonth) {
                                final Calendar ca = Calendar.getInstance();
                                ca.set(Calendar.YEAR,year);
                                ca.set(Calendar.MONTH,monthOfYear);
                                ca.set(Calendar.DAY_OF_MONTH,dayOfMonth);
                                mYear2=year;
                                mMonth2=monthOfYear;
                                mDay2=dayOfMonth;
                                et_todate.setText(sdf.format(ca.getTime()));
                            }
                        }, mYear2, mMonth2, mDay2);
                dpd.show();
            }
        });
        bt_cancel=(Button)findViewById(R.id.bt_cancel);
        bt_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                layout.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, 0));
            }
        });
        CheckConnection();

    }


    public void CheckConnection()
    {
        //Check network
        Utils.context=this;
        if(!Utils.isConnectingToInternet()) {
            final AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setMessage(getResources().getString(R.string.requiredConnect))
                    .setPositiveButton(getResources().getString(R.string.retry), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            CheckConnection();
                        }
                    })
                    .setNegativeButton(getResources().getString(R.string.close), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            dialogInterface.dismiss();
                        }
                    }).show();
        }
        else
        {
            RequestItemsServiceTask task = new RequestItemsServiceTask();
            task.execute();

        }
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_bill_store, menu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == android.R.id.home) {
            this.finish();
            return true;
        }
        if(id==R.id.action_createbill)
        {
            Intent intent_CreateBill = new Intent(BillStore.this,CreateBill.class);
            startActivity(intent_CreateBill);
        }
        if(id==R.id.action_filter)
        {
            layout.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT));
        }
        return super.onOptionsItemSelected(item);
    }

    private class RequestItemsServiceTask extends AsyncTask<Void, Void, Void> {

        private ProgressDialog dialog = new ProgressDialog(BillStore.this);

        String stringJSON=null;

        @Override
        protected void onPreExecute() {
            // TODO i18n
            dialog.setMessage(getResources().getString(R.string.please_wait));
            dialog.setButton(DialogInterface.BUTTON_NEGATIVE, getResources().getString(R.string.close), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                    cancel(true);

                }
            });
            dialog.show();
        }

        @Override
        protected Void doInBackground(Void... unused) {
            // The ItemService would contain the method showed
            // in the previous paragraph
            String surl = "http://salemh.skyit.vn/api/HoaDon/";
            try {
                stringJSON=Utils.makeRequest2("GET",surl,null,"application/json","");
//                Log.d("RESULT:",stringJSON);
            } catch (IOException e) {
                e.printStackTrace();
                stringJSON=null;
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void unused) {
            if(stringJSON!=null) {
                itemsList = new ArrayList<BillModel>();
                try {
                    JSONArray jsonArray = new JSONArray(stringJSON);
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject jsonObject = jsonArray.getJSONObject(i);
                        try {
                            BillModel bill = new BillModel();
                            bill.setId(jsonObject.getInt("id"));
                            bill.setNgayLap(jsonObject.getString("ngayLap").substring(0, 10));
                            bill.setTenKhachHang(jsonObject.getString("tenKhachHang"));
                            bill.setTenNhanVien(jsonObject.getString("tenNhanVien"));
                            bill.setIdKhachHang(jsonObject.getInt("idKhachHang"));
                            bill.setChietKhau(jsonObject.getDouble("chietKhau"));
                            bill.setSoHoaDon(jsonObject.getInt("soHoaDon"));
                            bill.setVAT(jsonObject.getInt("vat"));
                            bill.setGhiChu(jsonObject.getString("ghiChu"));
                            bill.setSoTienKhachDua(jsonObject.getInt("soTienKhachDua"));
                            bill.setLoaiHoaDon(jsonObject.getInt("loaiHoaDon"));
                            bill.setHinhThucThanhToan(jsonObject.getString("hinhThucThanhToan"));
                            bill.setTongTien(jsonObject.getInt("tongTien"));
                            itemsList.add(bill);
                        } catch (JSONException e) {
                            continue;
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                if(itemsList.size()!=0) {
                    Collections.reverse(itemsList);
                    billAdapter = new CustomAdapter_Bill(BillStore.this, itemsList);
                    lv.setAdapter(billAdapter);

                    usernameList.add(itemsList.get(0).getTenNhanVien());
                    for (int i = 0; i < itemsList.size(); i++) {
                        boolean addable = true;
                        for (int j = 0; j < usernameList.size(); j++) {
                            if (itemsList.get(i).getTenNhanVien().equals(usernameList.get(j))) {
                                addable = false;
                                break;
                            }
                            addable = true;
                        }
                        if (addable) {
                            usernameList.add(itemsList.get(i).getTenNhanVien());
                        }
                    }
                    customerList.add(itemsList.get(0).getTenKhachHang());
                    for (int i = 0; i < itemsList.size(); i++) {
                        boolean addable = true;
                        for (int j = 0; j < customerList.size(); j++) {
                            if (itemsList.get(i).getTenKhachHang().equals(customerList.get(j))) {
                                addable = false;
                                break;
                            }
                            addable = true;
                        }
                        if (addable) {
                            customerList.add(itemsList.get(i).getTenKhachHang());
                        }
                    }

                    ArrayAdapter<String> userAdapter = new ArrayAdapter<>(BillStore.this, android.R.layout.simple_list_item_1, usernameList);
                    act_username.setAdapter(userAdapter);
                    act_username.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            act_username.showDropDown();
                        }
                    });
                    ArrayAdapter<String> customerAdapter = new ArrayAdapter<>(BillStore.this, android.R.layout.simple_list_item_1, customerList);
                    act_customername.setAdapter(customerAdapter);
                    act_customername.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            act_customername.showDropDown();
                        }
                    });
                    lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                            Intent detail = new Intent(BillStore.this, Bill_Detail.class);
                            detail.putExtra("bill", billAdapter.lbill.get(i));
                            startActivity(detail);
                        }
                    });
                    sv.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
                        android.widget.Filter filter = billAdapter.getFilter();

                        @Override
                        public boolean onQueryTextSubmit(String query) {
                            return false;
                        }

                        @Override
                        public boolean onQueryTextChange(String newText) {
                            if (TextUtils.isEmpty(newText)) {
                                filter.filter("");
                            } else {
                                //lv.setFilterText(newText);
                                filter.filter(newText);
                            }
                            return true;
                        }
                    });
                    if (dialog.isShowing()) {
                        dialog.dismiss();
                    }
                }
                else
                    dialog.setMessage(getResources().getString(R.string.empty_data));
            }

            else
                dialog.setMessage(getResources().getString(R.string.requiredServer));
        }
    }
}

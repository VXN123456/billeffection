package vienuser.example.com.billeffection.models;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by KP on 09/05/2016.
 */
public class ProductTypeModel implements Parcelable {
    private int id;
    private String tenLoaiSanPham;
    public ProductTypeModel() {
        id = 0;
        tenLoaiSanPham = "";
    }

    public static final Creator<ProductTypeModel> CREATOR = new Creator<ProductTypeModel>() {
        @Override
        public ProductTypeModel createFromParcel(Parcel in) {
            return new ProductTypeModel();
        }

        @Override
        public ProductTypeModel[] newArray(int size) {
            return new ProductTypeModel[size];
        }
    };

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }



    public String getTenLoaiSanPham() {
        return tenLoaiSanPham;
    }

    public void setTenLoaiSanPham(String tenLoaiSanPham) {
        this.tenLoaiSanPham = tenLoaiSanPham;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(id);
        parcel.writeString(tenLoaiSanPham);
    }
}

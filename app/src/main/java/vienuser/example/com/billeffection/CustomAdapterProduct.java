package vienuser.example.com.billeffection;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import vienuser.example.com.billeffection.models.ProductModel;
import vienuser.example.com.billeffection.models.Utils;

/**
 * Created by VienUser on 5/5/2016.
 */
public class CustomAdapterProduct extends ArrayAdapter implements Filterable {


    //int quantity;
    List<ProductModel> productModels1;
    List<ProductModel> productModelList;
    LayoutInflater inflater;
    int resource;

    public CustomAdapterProduct(Context context, int resource, List<ProductModel> objects) {
        super(context, resource, objects);
        this.productModelList = objects;
        this.resource = resource;
        inflater = (LayoutInflater) context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
    }

    //filter data
    public Filter getFilter() {
        return new Filter() {

            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                final FilterResults oReturn = new FilterResults();
                final ArrayList<ProductModel> results = new ArrayList<ProductModel>();
                if (productModels1 == null)
                    productModels1 =  productModelList;
                if (constraint != null) {
                    if (productModels1 != null && productModels1.size() > 0) {
                        for (final ProductModel productModel : productModels1) {
                            if (Utils.ConvertString(productModel.getTenSanPham().toLowerCase())
                                    .contains(Utils.ConvertString(constraint.toString())) || productModel.getMaSanPham().contains(constraint.toString()))
                                results.add(productModel);
                        }
                    }
                    oReturn.values = results;
                }
                return oReturn;
            }

            @SuppressWarnings("unchecked")
            @Override
            protected void publishResults(CharSequence constraint,
                                          FilterResults results) {
                productModelList = (ArrayList<ProductModel>) results.values;
                notifyDataSetChanged();
            }
        };
    }


    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return productModelList.size();
    }

    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return position;
    }


    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        if (convertView == null)
        {
            convertView = inflater.inflate(resource,null);
        }


        TextView tvNameSP = (TextView)convertView.findViewById(R.id.tv_ProductName);
        TextView tvPriceSP = (TextView)convertView.findViewById(R.id.tv_ProductPrice);
        TextView tvQuantity = (TextView)convertView.findViewById(R.id.tv_Quantity);
        TextView tvSTT = (TextView)convertView.findViewById(R.id.tv_STT) ;
        TextView tvCode = (TextView)convertView.findViewById(R.id.tv_CodeProduct) ;
        TextView tvPriceSemiFinal = (TextView)convertView.findViewById(R.id.tv_PricePerProduct) ;
        TextView tvCK = (TextView)convertView.findViewById(R.id.tv_ChiecKhau);
        TextView tvGG = (TextView)convertView.findViewById(R.id.tv_GG);

        if (tvQuantity!=null){
            tvQuantity.setText(Utils.ConvertCurrency(String.valueOf(CreateBill.productModels.get(position).getSl())));
        }
        if (tvSTT!=null){
            tvSTT.setText(String.valueOf(CreateBill.productModels.get(position).getStt()));
        }
        if (tvPriceSemiFinal!=null){
            float orig_Price = CreateBill.productModels.get(position).getGiaSiCapMot()*CreateBill.productModels.get(position).getSl();
            float ck = orig_Price*CreateBill.productModels.get(position).getChieuKhau()/100;
            int gg = CreateBill.productModels.get(position).getSl()*CreateBill.productModels.get(position).getGiamGia();
            tvPriceSemiFinal.setText(Utils.ConvertCurrency(String.valueOf((int)(orig_Price-ck-gg))));
        }
        if (tvCK!=null){
            tvCK.setText(String.valueOf(CreateBill.productModels.get(position).getChieuKhau()));
        }
        if (tvGG!=null){
            tvGG.setText(Utils.ConvertCurrency(String.valueOf(CreateBill.productModels.get(position).getGiamGia())));
        }
        tvNameSP.setText(productModelList.get(position).getTenSanPham());
        if (CreateBill.price==1){
            tvPriceSP.setText(Utils.ConvertCurrency(String.valueOf((int) (productModelList.get(position).getGiaBan()))));
        }
        if (CreateBill.price==2){
            tvPriceSP.setText(Utils.ConvertCurrency(String.valueOf((int)(productModelList.get(position).getGiaSiCapMot()))));
        }
        if (CreateBill.price==3){
            tvPriceSP.setText(Utils.ConvertCurrency(String.valueOf((int)(productModelList.get(position).getGiaSiCapHai()))));
        }
        if (tvCode!= null){
            tvCode.setText(String.valueOf(productModelList.get(position).getMaSanPham()));
        }

        return convertView;
    }
}


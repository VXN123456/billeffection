package vienuser.example.com.billeffection;

import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.text.Editable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import vienuser.example.com.billeffection.models.ProductModel;
import vienuser.example.com.billeffection.models.Utils;

/**
 * Created by VienUser on 5/18/2016.
 */
public class ProductEdit extends DialogFragment {
    private ProductModel productModel;
    Dialog dialog;
    EditText etGG;
    EditText etCK;
    EditText etDG;
    EditText etSL;
    TextView tvPName;
    CreateBill context;
    int position;
    String sl;
    String ck;
    String gg;
    String dg;

    public ProductEdit() {

    }
    public interface EditNameDialogListener {
//        void onFinishEditDialog(String t,String V);
        void onFinishEditDialog(ProductModel productModel, String position);
    }
//    EditNameDialogListener activity;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {



        Intent intent = getActivity().getIntent();
        productModel = intent.getParcelableExtra("product");
        position = intent.getIntExtra("position",0);
        Log.d("name",productModel.getTenSanPham());
        Log.d("position", String.valueOf(position));

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();

        View view = inflater.inflate(R.layout.dialog_edit_product,null);
        builder.setView(view);

        tvPName = (TextView)view.findViewById(R.id.tv_ProductName) ;
        etSL = (EditText)view.findViewById(R.id.et_EditQuantity);
        etDG = (EditText)view.findViewById(R.id.et_EditPrice);
        etCK = (EditText)view.findViewById(R.id.et_EditCK);
        etGG = (EditText)view.findViewById(R.id.et_EditGG) ;
        //set
        tvPName.setText(productModel.getTenSanPham());
        etSL.setText(String.valueOf(productModel.getSl()));
        etDG.setText(String.valueOf((int)productModel.getGiaSiCapMot()));
        etCK.setText(String.valueOf(productModel.getChieuKhau()));
        etGG.setText(String.valueOf(productModel.getGiamGia()));

        //button
        Button btnOK = (Button)view.findViewById(R.id.btn_OK);
        Button btnCancel = (Button)view.findViewById(R.id.btn_Cancel);

        btnOK.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (etSL.getText().length()==0||((int)Float.parseFloat(etSL.getText().toString()))<=0){
                    final android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(getActivity());
                    builder.setMessage(getResources().getString(R.string.missing_data))
                        .setPositiveButton(getResources().getString(R.string.close), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                dialogInterface.dismiss();
                            }
                        }).show();
                }
                else{
                    sl = String.valueOf(etSL.getText());
                    dg = String.valueOf(etDG.getText());
                    ck = String.valueOf(etCK.getText());
                    gg = String.valueOf(etGG.getText());

                    if (dg.length()==0){dg="0";}
                    if (ck.length()==0){ck="0";}
                    if (gg.length()==0){gg="0";}
                    productModel.setSl((int) Float.parseFloat(sl));
                    productModel.setGiaSiCapMot(Float.parseFloat(dg));
                    productModel.setChieuKhau(Float.parseFloat(ck));
                    productModel.setGiamGia((int) Float.parseFloat(gg));
                    CreateBill createBill = (CreateBill)getActivity();
                    createBill.onFinishEditDialog(productModel, String.valueOf(position));
                    createBill.updateProduct();
                    createBill.onResume();
                    getDialog().dismiss();
                }

            }
        });
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog = builder.create();

        return dialog;
    }

}

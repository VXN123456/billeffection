package vienuser.example.com.billeffection;

import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.SearchView;

import java.util.List;

import vienuser.example.com.billeffection.models.CustomerModel;

/**
 * Created by VienUser on 5/13/2016.
 */
public class SearchCustomer extends android.support.v4.app.Fragment {

     public String customerName;
    String customerPhone;
    String customerAddress;
    String customerCode;
    String customerId ;
    SearchView svCustomer;
    ListView lv_SearchCustomer;
    CustomAdapterCustomer searchAdapterCustomer;
    List<CustomerModel> searchCustomerModels;

    public void onAttach(CreateBill createBill) {
        Log.d("zzz","Fragment has attach");
        super.onAttach(createBill);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        Log.d("zzz","Fragment onCreate");
        CreateBill.isExistSearchCustomer = true;
        super.onCreate(savedInstanceState);
    }

    public void onDetach() {
        Log.d("fragment","onDetach");
        super.onDetach();
    }


    @Override
    public View onCreateView(LayoutInflater inflater, final ViewGroup container,
                             final Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view =  inflater.inflate(R.layout.search_customer,
                container, false);

        svCustomer = (SearchView)view.findViewById(R.id.sv_SearchCustomer);
        lv_SearchCustomer = (ListView)view.findViewById(R.id.lv_SearchCustomer);

        final Bundle bundle = getArguments();
        searchCustomerModels =  bundle.getParcelableArrayList("data_Customer");
        searchAdapterCustomer = new CustomAdapterCustomer((CreateBill)getActivity(),R.layout.row_customer,searchCustomerModels);
        lv_SearchCustomer.setAdapter(searchAdapterCustomer);

        svCustomer.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            android.widget.Filter filter = searchAdapterCustomer.getFilter();
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                if (TextUtils.isEmpty(newText)) {
                    filter.filter("");
                } else {
                    //lv.setFilterText(newText);
                    filter.filter(newText);
                }
                return true;
            }
        });
        lv_SearchCustomer.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                customerName = searchAdapterCustomer.customerModelList.get(position).getTenKH();
                customerPhone = searchAdapterCustomer.customerModelList.get(position).getSdt();
                customerAddress = searchAdapterCustomer.customerModelList.get(position).getDiaChi();
                customerCode = searchAdapterCustomer.customerModelList.get(position).getMaKH();
                customerId = String.valueOf(searchAdapterCustomer.customerModelList.get(position).getId());
                CreateBill createBill = (CreateBill)getActivity();
                createBill.attv_Product.setEnabled(true);
                createBill.attv_Product.requestFocus();
                createBill.setDataCustomer(customerName,customerPhone,customerAddress,customerId);
                createBill.removeFragmentSearchCustomer();
                CreateBill.isExistSearchCustomer = false;
                createBill.onBackPressed();
                createBill.onResume();
            }
        });
        return view;
    }


}

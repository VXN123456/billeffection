package vienuser.example.com.billeffection;

import android.app.FragmentManager;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.Toast;

import java.util.List;

import vienuser.example.com.billeffection.models.ProductModel;

/**
 * Created by VienUser on 5/16/2016.
 */
public class SearchProduct extends android.support.v4.app.Fragment {
    String productCode;
    FragmentManager fragmentManager;
    SearchView svProduct;
    ListView lv_SearchProduct;
    CustomAdapterProduct searchAdapterProduct;
    List<ProductModel> searchProductModels;

    public void onAttach(CreateBill createBill) {
        Log.d("zzz","Fragment attached");
        super.onAttach(createBill);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        Log.d("zzz","Fragment onCreate");
        CreateBill.isExistSearchProduct = true;
        super.onCreate(savedInstanceState);
    }

    public void onDetach() {
        Log.d("fragment","onDetach");
        super.onDetach();
    }


    @Override
    public View onCreateView(LayoutInflater inflater, final ViewGroup container,
                             final Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view =  inflater.inflate(R.layout.search_product,
                container, false);

        svProduct = (SearchView)view.findViewById(R.id.sv_Product);
        lv_SearchProduct = (ListView)view.findViewById(R.id.lv_SearchProduct);

        final Bundle bundle = getArguments();
        searchProductModels =  bundle.getParcelableArrayList("data_Product");
        searchAdapterProduct = new CustomAdapterProduct(getActivity(),R.layout.row_product,searchProductModels);
        lv_SearchProduct.setAdapter(searchAdapterProduct);

        svProduct.setOnQueryTextListener(new android.widget.SearchView.OnQueryTextListener() {
            android.widget.Filter filter = searchAdapterProduct.getFilter();
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }
            @Override
            public boolean onQueryTextChange(String newText) {
                if (TextUtils.isEmpty(newText)) {
                    filter.filter("");
                } else {
                    //lv.setFilterText(newText);
                    filter.filter(newText);
                }
                return true;
            }
        });
        lv_SearchProduct.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//                        Toast.makeText(CreateBill.this, "you are clicked " + result.get(position).getMaSanPham(), Toast.LENGTH_SHORT).show();
                CreateBill createBill = (CreateBill)getActivity();
                if (createBill.getDataCustomerName().equals(""))
                {
                    Toast.makeText(getActivity(),"Vui lòng chọn khách hàng trước !",Toast.LENGTH_SHORT).show();
                }
                else
                {
                    String productName = searchAdapterProduct.productModelList.get(position).getTenSanPham();
                    int productQuantity = 1;
                    float productPrice = 0;
                    if (CreateBill.price==1){
                         productPrice = searchAdapterProduct.productModelList.get(position).getGiaBan();
                    }
                    if (CreateBill.price==2){
                         productPrice = searchAdapterProduct.productModelList.get(position).getGiaSiCapMot();
                    }
                    if (CreateBill.price==3){
                         productPrice = searchAdapterProduct.productModelList.get(position).getGiaSiCapHai();
                    }
                    float productCK = 0;
                    int pID = searchAdapterProduct.productModelList.get(position).getId();
                    String pCode = searchAdapterProduct.productModelList.get(position).getMaSanPham();
                    createBill.etSL.requestFocus();
                    createBill.setDataProduct(productName,productPrice,productQuantity,productCK,pID,pCode);
                    createBill.removeFragmentSearchProduct();
                    CreateBill.isExistSearchProduct = false;
                    createBill.onBackPressed();
                    createBill.resetView();
                    createBill.onResume();
                }
            }
        });
        return view;
    }

}

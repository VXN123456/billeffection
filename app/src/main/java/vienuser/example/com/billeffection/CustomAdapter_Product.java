package vienuser.example.com.billeffection;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import java.util.ArrayList;

import vienuser.example.com.billeffection.models.ProductModel;
import vienuser.example.com.billeffection.models.ProductTypeModel;
import vienuser.example.com.billeffection.models.Utils;

/**
 * Created by KP on 06/05/2016.
 */
public class CustomAdapter_Product extends BaseAdapter implements Filterable {
    ArrayList<ProductModel> lproduct;
    ArrayList<ProductModel> orig;
    ArrayList<ProductTypeModel> lproducttype;
    Context context;
    ProductStore activity;
    private LayoutInflater inflater = null;

    public CustomAdapter_Product(ProductStore mainActivity, ArrayList<ProductModel> al, ArrayList<ProductTypeModel> altype) {
        // TODO Auto-generated constructor stub
        lproduct = al;
        lproducttype=altype;
        context = mainActivity;
        activity=mainActivity;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public Filter getFilter() {
        return new Filter() {

            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                final FilterResults oReturn = new FilterResults();
                final ArrayList<ProductModel> results = new ArrayList<ProductModel>();
                if (orig == null)
                    orig = lproduct;
                if (constraint != null) {
                    if (orig != null && orig.size() > 0) {
                        for (final ProductModel g : orig) {
                            if (Utils.ConvertString(g.getTenSanPham().toString().toLowerCase())
                                    .contains(Utils.ConvertString(constraint.toString())) || g.getMaSanPham().toString().contains(constraint.toString()))
                                results.add(g);
                        }
                    }
                    oReturn.values = results;
                }
                return oReturn;
            }

            @SuppressWarnings("unchecked")
            @Override
            protected void publishResults(CharSequence constraint,
                                          FilterResults results) {
                lproduct = (ArrayList<ProductModel>) results.values;
                notifyDataSetChanged();
            }
        };
    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return lproduct.size();
    }

    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    public class Holder {
        TextView tvname;
        TextView tvid;
        TextView tvgiasi;
        TextView tvquantity;
    }
    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub
        Holder holder = new Holder();
        if(convertView==null) {
            convertView = inflater.inflate(R.layout.custom_listview_product_store, null);
        }
        holder.tvid = (TextView) convertView.findViewById(R.id.tv_id);
        holder.tvname = (TextView) convertView.findViewById(R.id.tv_name);
        holder.tvgiasi=(TextView)convertView.findViewById(R.id.tv_giasi);
        holder.tvquantity=(TextView)convertView.findViewById(R.id.tv_quantity);

        holder.tvname.setText(lproduct.get(position).getTenSanPham());
        holder.tvid.setText(lproduct.get(position).getMaSanPham());
        holder.tvgiasi.setText( Utils.ConvertCurrency(Integer.toString(Math.round(lproduct.get(position).getGiaSiCapMot()))));
        holder.tvquantity.setText(Utils.ConvertCurrency(Integer.toString(Math.round(lproduct.get(position).getSoLuong()))));

        return convertView;
    }
}

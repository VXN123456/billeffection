package vienuser.example.com.billeffection;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import vienuser.example.com.billeffection.models.ProductModel;
import vienuser.example.com.billeffection.models.Utils;

/**
 * Created by KP on 17/05/2016.
 */
public class customadapter extends ArrayAdapter implements Filterable {


    //int quantity;
    List<ProductModel> productModels1;
    List<ProductModel> productModelList;
    LayoutInflater inflater;
    int resource;
    CreateBill content;
    public customadapter(CreateBill content, int resource, List<ProductModel> objects) {
        super(content, resource, objects);
        this.productModelList = objects;
        this.content=content;
        this.resource = resource;
        inflater = (LayoutInflater) content.getSystemService(content.LAYOUT_INFLATER_SERVICE);
    }

    //filter data
    public Filter getFilter() {
        return new Filter() {

            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                final FilterResults oReturn = new FilterResults();
                final ArrayList<ProductModel> results = new ArrayList<ProductModel>();
                if(constraint=="" || constraint==null)
                    constraint="";
                if (productModels1 == null)
                    productModels1 =  productModelList;
                if (constraint != null) {
                    if (productModels1 != null && productModels1.size() > 0) {
                        for (final ProductModel productModel : productModels1) {
                            if (Utils.ConvertString(productModel.getTenSanPham().toLowerCase())
                                    .contains(Utils.ConvertString(constraint.toString())))
                                results.add(productModel);
                        }
                    }
                    oReturn.values = results;
                    oReturn.count=results.size();
                }
                return oReturn;
            }

            @SuppressWarnings("unchecked")
            @Override
            protected void publishResults(CharSequence constraint,
                                          FilterResults results) {
                productModelList = (ArrayList<ProductModel>) results.values;
                notifyDataSetChanged();
            }
        };
    }


    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return productModelList.size();
    }

    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        //content.setDataProduct(productModelList.get(position).getTenSanPham(),productModelList.get(position).getGiaSiCapMot(),1,0);

        return productModelList.get(position).getTenSanPham();
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return position;
    }


    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        if (convertView == null)
        {
            convertView = inflater.inflate(resource,null);
        }
        TextView tvNameSP = (TextView)convertView.findViewById(R.id.tv_ProductName);
        TextView tvPriceSP = (TextView)convertView.findViewById(R.id.tv_ProductPrice);
        TextView tvCode = (TextView)convertView.findViewById(R.id.tv_CodeProduct) ;

        tvNameSP.setText(productModelList.get(position).getTenSanPham());
        if (CreateBill.price==1){
            tvPriceSP.setText(Utils.ConvertCurrency(String.valueOf(Math.round(productModelList.get(position).getGiaBan()))));
        }
        if (CreateBill.price==2){
            tvPriceSP.setText(Utils.ConvertCurrency(String.valueOf(Math.round(productModelList.get(position).getGiaSiCapMot()))));
        }
        if (CreateBill.price==3){
            tvPriceSP.setText(Utils.ConvertCurrency(String.valueOf(Math.round(productModelList.get(position).getGiaSiCapHai()))));
        }
        if (tvCode!= null){
            tvCode.setText(String.valueOf(productModelList.get(position).getMaSanPham()));
        }

        return convertView;
    }
}


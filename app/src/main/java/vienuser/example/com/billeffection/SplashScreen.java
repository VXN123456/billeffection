package vienuser.example.com.billeffection;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import vienuser.example.com.billeffection.models.DBHelper;
import vienuser.example.com.billeffection.models.UserModel;
import vienuser.example.com.billeffection.models.Utils;

public class SplashScreen extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);

    }
    @Override
    public void onResume()
    {  // After a pause OR at startup
        super.onResume();
        //Check network
        Utils.context=this;
        if(!Utils.isConnectingToInternet()) {
            final AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setMessage(getResources().getString(R.string.requiredConnect))
                    .setPositiveButton(getResources().getString(R.string.retry), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            onResume();
                        }
                    })
                    .setNegativeButton(getResources().getString(R.string.close), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            dialogInterface.dismiss();
                        }
                    }).show();
        }
        else
        {

            new LoginTask().execute();

        }
    }
    public class LoginTask extends AsyncTask<Void, Void, Void> {
        String stringJSON = null;
        List<UserModel> userModelList;
        private ProgressDialog dialog = new ProgressDialog(SplashScreen.this);
        boolean ok=false;
        AsyncTask task;
        DBHelper db = new DBHelper(getApplication());
        Cursor resultSet = db.getData();
        @Override
        protected void onPreExecute() {
            task=this;
        }

        @Override
        protected Void doInBackground(Void... params) {

            String surl = "http://salemh.skyit.vn/api/NhanVien/";
            try {
                stringJSON = Utils.makeRequest2("GET", surl, null, "application/json", "");
                Log.d("RESULT:", stringJSON);
            } catch (IOException e) {
                e.printStackTrace();
                stringJSON = null;
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void unused) {

            if (stringJSON != null) {
                if (resultSet.getCount() != 0) {
                    resultSet.moveToFirst();
                    String username = resultSet.getString(0);
                    String password = resultSet.getString(1);
                    try {
                        JSONArray jsonArrayParent = new JSONArray(stringJSON);
                        userModelList = new ArrayList<>();
                        for (int i = 0; i < jsonArrayParent.length(); i++) {
                            JSONObject objectFinal = jsonArrayParent.getJSONObject(i);
                            try {
                                UserModel userModel = new UserModel();
                                userModel.setTenDangNhap(objectFinal.getString("tenDangNhap"));
                                userModel.setMatKhau(objectFinal.getString("matKhau"));
                                userModel.setId(objectFinal.getInt("id"));
                                userModel.setTenNV(objectFinal.getString("tenNhanVien"));

                                userModelList.add(userModel);
                            } catch (JSONException e) {
                                continue;
                            }
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    for (UserModel userModel : userModelList) {
                        if (username.equals(userModel.getTenDangNhap()) && password.equals(userModel.getMatKhau())) {
                            int id = userModel.getId();
                            Intent intent_Login = new Intent(SplashScreen.this, LoginSuccess.class);
                            intent_Login.putExtra("data_Username", userModel.getTenDangNhap());
                            intent_Login.putExtra("data_Name", userModel.getTenNV());
                            intent_Login.putExtra("data_IdUser", String.valueOf(id));
                            startActivity(intent_Login);
                            ok=true;
                            SystemClock.sleep(2000);
                            finish();
                            break;
                        }
                    }
                    if(!ok)
                    {
                        SystemClock.sleep(2000);
                        Intent i = new Intent(SplashScreen.this, Login.class);
                        startActivity(i);
                        finish();
                    }
                } else {
                    SystemClock.sleep(2000);
                    Intent i = new Intent(SplashScreen.this, Login.class);
                    startActivity(i);
                    finish();
                }
            } else {
                dialog.setMessage(getResources().getString(R.string.requiredServer));
                dialog.setButton(DialogInterface.BUTTON_NEGATIVE, getResources().getString(R.string.retry), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        SplashScreen.this.onResume();

                    }
                });
                dialog.show();

            }

        }
    }
}

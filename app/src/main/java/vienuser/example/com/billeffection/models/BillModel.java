package vienuser.example.com.billeffection.models;

import java.io.Serializable;

/**
 * Created by KP on 26/04/2016.
 */
public class BillModel implements Serializable {
    Integer id;
    String tenKhachHang;
    String tenNhanVien;
    Integer SoHoaDon;
    Integer VAT;
    Integer SoTienKhachDua;
    Integer LoaiHoaDon;
    Double ChietKhau;
    String GhiChu,HinhThucThanhToan;
    String NgayLap;

    public Integer getTongTien() {
        return TongTien;
    }

    public void setTongTien(Integer tongTien) {
        TongTien = tongTien;
    }

    Integer TongTien;
    private int idKhachHang;
    private int idNhanVien;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTenKhachHang() {
        return tenKhachHang;
    }

    public void setTenKhachHang(String tenKhachHang) {
        this.tenKhachHang = tenKhachHang;
    }

    public String getTenNhanVien() {
        return tenNhanVien;
    }

    public void setTenNhanVien(String tenNhanVien) {
        this.tenNhanVien = tenNhanVien;
    }

    public Integer getSoHoaDon() {
        return SoHoaDon;
    }

    public void setSoHoaDon(Integer soHoaDon) {
        SoHoaDon = soHoaDon;
    }

    public Integer getVAT() {
        return VAT;
    }

    public void setVAT(Integer VAT) {
        this.VAT = VAT;
    }

    public Integer getSoTienKhachDua() {
        return SoTienKhachDua;
    }

    public void setSoTienKhachDua(Integer soTienKhachDua) {
        SoTienKhachDua = soTienKhachDua;
    }


    public Integer getLoaiHoaDon() {
        return LoaiHoaDon;
    }

    public void setLoaiHoaDon(Integer loaiHoaDon) {
        LoaiHoaDon = loaiHoaDon;
    }

    public Double getChietKhau() {
        return ChietKhau;
    }

    public void setChietKhau(Double chietKhau) {
        ChietKhau = chietKhau;
    }

    public String getGhiChu() {
        return GhiChu;
    }

    public void setGhiChu(String ghiChu) {
        GhiChu = ghiChu;
    }

    public String getHinhThucThanhToan() {
        return HinhThucThanhToan;
    }

    public void setHinhThucThanhToan(String hinhThucThanhToan) {
        HinhThucThanhToan = hinhThucThanhToan;
    }

    public String getNgayLap() {
        return NgayLap;
    }

    public void setNgayLap(String ngayLap) {
        NgayLap = ngayLap;
    }

    public int getIdNhanVien() {
        return idNhanVien;
    }

    public void setIdNhanVien(int idNhanVien) {
        this.idNhanVien = idNhanVien;
    }

    public int getIdKhachHang() {
        return idKhachHang;
    }

    public void setIdKhachHang(int idKhachHang) {
        this.idKhachHang = idKhachHang;
    }
}

package vienuser.example.com.billeffection.models;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by VienUser on 4/27/2016.
 */
public class CustomerModel implements Parcelable {

    private int id;
    private String maKH;
    private String tenKH;
    private String diaChi;
    private String sdt;
    private String soFax;
    private String email;
    private double congNo;
    private String msThue;
    private String ngayNhap;
    private float congNoMax;
    private String tenString;
    private String hanTra;

    public CustomerModel() {
        id = 0;
        tenKH = "";
        sdt = "";
        diaChi = "";
        maKH = "";
        soFax="";
    }

    public CustomerModel(Parcel in) {
        id = in.readInt();
        tenKH = in.readString();
        sdt =  in.readString();
        diaChi =  in.readString();
        maKH =  in.readString();
        soFax=in.readString();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getMaKH() {
        return maKH;
    }

    public void setMaKH(String maKH) {
        this.maKH = maKH;
    }

    public String getTenKH() {
        return tenKH;
    }

    public void setTenKH(String tenKH) {
        this.tenKH = tenKH;
    }

    public String getDiaChi() {
        return diaChi;
    }

    public void setDiaChi(String diaChi) {
        this.diaChi = diaChi;
    }

    public String getSdt() {
        return sdt;
    }

    public void setSdt(String sdt) {
        this.sdt = sdt;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getMsThue() {
        return msThue;
    }

    public void setMsThue(String msThue) {
        this.msThue = msThue;
    }

    public String getNgayNhap() {
        return ngayNhap;
    }

    public void setNgayNhap(String ngayNhap) {
        this.ngayNhap = ngayNhap;
    }

    public float getCongNoMax() {
        return congNoMax;
    }

    public void setCongNoMax(float congNoMax) {
        this.congNoMax = congNoMax;
    }

    public String getTenString() {
        return tenString;
    }

    public void setTenString(String tenString) {
        this.tenString = tenString;
    }

    public String getHanTra() {
        return hanTra;
    }

    public void setHanTra(String hanTra) {
        this.hanTra = hanTra;
    }
    public String getSoFax() {
        return soFax;
    }

    public void setSoFax(String soFax) {
        this.soFax = soFax;
    }
    public void setCongNo(double congNo) {
        this.congNo = congNo;
    }
    public double getCongNo()
    {
        return congNo;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeString(tenKH);
        dest.writeString(sdt);
        dest.writeString(diaChi);
        dest.writeString(maKH);
        dest.writeString(soFax);
//        dest.writeString(email);
//        dest.writeString(msThue);
//        dest.writeString(ngayNhap);
//        dest.writeFloat(congNoMax);
//        dest.writeString(tenString);
//        dest.writeString(hanTra);
//        dest.writeDouble(congNo);
    }

    public static final Parcelable.Creator<CustomerModel> CREATOR = new Parcelable.Creator<CustomerModel>()
    {
        public CustomerModel createFromParcel(Parcel in)
        {
            return new CustomerModel(in);
        }
        public CustomerModel[] newArray(int size)
        {
            return new CustomerModel[size];
        }
    };

}

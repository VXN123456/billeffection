package vienuser.example.com.billeffection;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

import org.json.JSONObject;

import java.util.ArrayList;

import vienuser.example.com.billeffection.models.ProductModel;
import vienuser.example.com.billeffection.models.ProductTypeModel;
import vienuser.example.com.billeffection.models.Utils;

/**
 * Created by KP on 06/05/2016.
 */
public class ProductDetail extends DialogFragment {
    Dialog dialog;
    Button bt_ok;
    Button bt_cancel;
    JSONObject params;
    EditText et_tensp;
    EditText et_soluongsp;
    EditText et_donvisp;
    EditText et_motasp;
    EditText et_giasp;
    Spinner spin_loaisp;
    ProductModel product;
    ArrayList<ProductTypeModel> producttype;
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        dialog = new Dialog(getActivity(),android.R.style.Theme_Holo_Light_Dialog);
        dialog.setTitle(getResources().getString(R.string.productDetail));
        //dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        Intent i = getActivity().getIntent();
        product =  i.getParcelableExtra("product");
        producttype=i.getParcelableArrayListExtra("producttype");
        ArrayList<String> tenlsp = new ArrayList<String>();
        ArrayList<Integer> idpsl=new ArrayList<Integer>();
        for (ProductTypeModel pm:producttype) {
            tenlsp.add(pm.getTenLoaiSanPham());
            idpsl.add(pm.getId());
        };

        dialog.setContentView(R.layout.dialog_product_detail);
        bt_ok = (Button) dialog.findViewById(R.id.bt_ok);
        bt_cancel=(Button)dialog.findViewById(R.id.bt_cancel);

        et_tensp =(EditText)dialog.findViewById(R.id.et_tensp);
        et_soluongsp =(EditText)dialog.findViewById(R.id.et_soluongsp);
        et_giasp =(EditText)dialog.findViewById(R.id.et_giasp);
        et_donvisp =(EditText)dialog.findViewById(R.id.et_donvisp);
        spin_loaisp=(Spinner)dialog.findViewById(R.id.spin_loaisp);
        et_motasp =(EditText)dialog.findViewById(R.id.et_motasp);
        //Disable dropdown spinner
        spin_loaisp.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (motionEvent.getAction() == MotionEvent.ACTION_UP) {

                }
                return true;
            }
        });
        et_tensp.setKeyListener(null);
        et_soluongsp.setKeyListener(null);
        et_giasp.setKeyListener(null);
        et_donvisp.setKeyListener(null);
        //spin_loaisp.setEnabled(false);
        et_motasp.setKeyListener(null);

        ViewGroup layout = (ViewGroup)bt_ok.getParent();
        layout.removeView(bt_ok);
        //set spinner
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item,tenlsp);
        spin_loaisp.setAdapter(dataAdapter);
        //set data
        et_motasp.setText(product.getMoTa());
        et_tensp.setText(product.getTenSanPham());
        et_soluongsp.setText(Utils.ConvertCurrency(String.valueOf((int) (product.getSoLuong()))));
        et_giasp.setText(Utils.ConvertCurrency(String.valueOf((int)(product.getGiaSiCapMot()))));
        et_donvisp.setText(product.getDonVi());
        spin_loaisp.setSelection(idpsl.indexOf(product.getIdLoaiSanPham()));



        bt_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
        return dialog;
    }

}

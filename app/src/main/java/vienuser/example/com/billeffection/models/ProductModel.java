package vienuser.example.com.billeffection.models;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by VienUser on 4/28/2016.
 */
public class ProductModel implements Parcelable{
    private int id;
    private String maSanPham;
    private String tenSanPham;
    private String donVi;
    private float giaBan;
    private float giaNhap;
    private float soLuong;
    private int idLoaiSanPham;
    private String moTa;
    private String ngayTao;
    private float giaSiCapMot;
    private float giaSiCapHai;
    private int sl;
    private int stt;
    private float chieuKhau;
    private  int giamGia;

    public int getGiamGia() {
        return giamGia;
    }
    public void setGiamGia(int giamGia) {
        this.giamGia = giamGia;
    }
    public float getChieuKhau() {
        return chieuKhau;
    }

    public void setChieuKhau(float chieuKhau) {
        this.chieuKhau = chieuKhau;
    }

    public int getStt() {
        return stt;
    }

    public void setStt(int stt) {
        this.stt = stt;
    }

    public int getSl() {
        return sl;
    }

    public void setSl(int sl) {
        this.sl = sl;
    }

    public ProductModel() {
        id = 0;
        tenSanPham = "";
        giaSiCapMot = 0;
        maSanPham = "";
        sl = 1;
        chieuKhau = 0;
        giamGia = 0;
    }

    public ProductModel(Parcel in) {
        id = in.readInt();
        tenSanPham = in.readString();
        giaSiCapMot =  in.readFloat();
        maSanPham = in.readString();
        sl = in.readInt();
        chieuKhau = in.readFloat();
        giamGia = in.readInt();

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getMaSanPham() {
        return maSanPham;
    }

    public void setMaSanPham(String maSanPham) {
        this.maSanPham = maSanPham;
    }


    public String getTenSanPham() {
        return tenSanPham;
    }

    public void setTenSanPham(String tenSanPham) {
        this.tenSanPham = tenSanPham;
    }

    public String getDonVi() {
        return donVi;
    }

    public void setDonVi(String donVi) {
        this.donVi = donVi;
    }

    public float getGiaBan() {
        return giaBan;
    }

    public void setGiaBan(float giaBan) {
        this.giaBan = giaBan;
    }

    public float getGiaNhap() {
        return giaNhap;
    }

    public void setGiaNhap(float giaNhap) {
        this.giaNhap = giaNhap;
    }

    public float getSoLuong() {
        return soLuong;
    }

    public void setSoLuong(float soLuong) {
        this.soLuong = soLuong;
    }

    public int getIdLoaiSanPham() {
        return idLoaiSanPham;
    }

    public void setIdLoaiSanPham(int idLoaiSanPham) {
        this.idLoaiSanPham = idLoaiSanPham;
    }

    public String getMoTa() {
        return moTa;
    }

    public void setMoTa(String moTa) {
        this.moTa = moTa;
    }

    public String getNgayTao() {
        return ngayTao;
    }

    public void setNgayTao(String ngayTao) {
        this.ngayTao = ngayTao;
    }

    public float getGiaSiCapMot() {
        return giaSiCapMot;
    }

    public void setGiaSiCapMot(float giaSiCapMot) {
        this.giaSiCapMot = giaSiCapMot;
    }

    public float getGiaSiCapHai() {
        return giaSiCapHai;
    }

    public void setGiaSiCapHai(float giaSiCapHai) {
        this.giaSiCapHai = giaSiCapHai;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeString(tenSanPham);
        dest.writeFloat(giaSiCapMot);
        dest.writeString(maSanPham);
        dest.writeInt(sl);
        dest.writeFloat(chieuKhau);
        dest.writeInt(giamGia);
    }

    public static final Parcelable.Creator<ProductModel> CREATOR = new Parcelable.Creator<ProductModel>()
    {
        public ProductModel createFromParcel(Parcel in)
        {
            return new ProductModel(in);
        }
        public ProductModel[] newArray(int size)
        {
            return new ProductModel[size];
        }
    };

}

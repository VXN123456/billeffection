package vienuser.example.com.billeffection;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import java.util.ArrayList;

import vienuser.example.com.billeffection.models.CustomerModel;
import vienuser.example.com.billeffection.models.Utils;

/**
 * Created by KP on 29/04/2016.
 */
public class CustomAdapter_Customer extends BaseAdapter implements Filterable {
    ArrayList<CustomerModel> lCus;
    ArrayList<CustomerModel> orig;
    Context context;
    CustomerStore activity;
    private LayoutInflater inflater = null;

    public CustomAdapter_Customer(CustomerStore mainActivity, ArrayList<CustomerModel> al) {
        // TODO Auto-generated constructor stub
        lCus = al;
        context = mainActivity;
        activity=mainActivity;

        inflater = (LayoutInflater) context.
                getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }


    public Filter getFilter() {
        return new Filter() {

            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                final FilterResults oReturn = new FilterResults();
                final ArrayList<CustomerModel> results = new ArrayList<CustomerModel>();
                if (orig == null)
                    orig = lCus;
                if (constraint != null) {
                    if (orig != null && orig.size() > 0) {
                        for (final CustomerModel g : orig) {
                            if (Utils.ConvertString(g.getTenKH().toString().toLowerCase())
                                    .contains(Utils.ConvertString(constraint.toString()))
                                    || g.getSdt().contains(constraint.toString()))
                                results.add(g);
                        }
                    }
                    oReturn.values = results;
                }
                return oReturn;
            }

            @SuppressWarnings("unchecked")
            @Override
            protected void publishResults(CharSequence constraint,
                                          FilterResults results) {
                lCus = (ArrayList<CustomerModel>) results.values;
                notifyDataSetChanged();
            }
        };
    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return lCus.size();
    }

    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    public class Holder {
        TextView tvname;
        TextView tvid;
        TextView tvsdt;
        TextView tvaddress;
    }
    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub
        Holder holder = new Holder();
        //View rowView;
        if(convertView==null) {
            convertView = inflater.inflate(R.layout.custom_listview_customer_store, null);
        }
        holder.tvid = (TextView) convertView.findViewById(R.id.tv_id);
        holder.tvname = (TextView) convertView.findViewById(R.id.tv_name);
        holder.tvsdt=(TextView) convertView.findViewById(R.id.tv_sdt);
        holder.tvaddress=(TextView)convertView.findViewById(R.id.tv_address);

        holder.tvname.setText(lCus.get(position).getTenKH());
        holder.tvid.setText(lCus.get(position).getMaKH());
        holder.tvsdt.setText(lCus.get(position).getSdt());
        holder.tvaddress.setText("ĐC: "+lCus.get(position).getDiaChi());

        return convertView;
    }

}

package vienuser.example.com.billeffection;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

import vienuser.example.com.billeffection.models.DBHelper;

public class LoginSuccess extends AppCompatActivity {

    public static String strId;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_success);
        Toolbar toolbar = (Toolbar) findViewById(R.id.app_bar_login_success);
        setSupportActionBar(toolbar);

        ImageButton btnCreateBill = (ImageButton)findViewById(R.id.ibtn_CreateBill);
        ImageButton btnTK = (ImageButton)findViewById(R.id.ibtn_ThongKeHoaDon);
        ImageButton btnCustomer=(ImageButton)findViewById(R.id.ibtn_Customer);
        ImageButton btnProduct = (ImageButton)findViewById(R.id.ibtn_Product);
        final TextView tvUsername = (TextView)findViewById(R.id.tv_UserLogin);

        Intent i = getIntent();
        String username = i.getStringExtra("data_Username");
        String data_Name = i.getStringExtra("data_Name");
        String data_IdUser = i.getStringExtra("data_IdUser");

        strId = data_IdUser;

//        Log.d("Id",data_IdUser);
//
//        Intent intent = new Intent(LoginSuccess.this,CreateBill.class);
//        intent.putExtra("data_Name",data_Name);
//        intent.putExtra("data_IdUser",String.valueOf(data_IdUser));


        if (tvUsername != null) {
            tvUsername.setText(username);
        }

        if (btnCreateBill != null) {
            btnCreateBill.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent_CreateBill = new Intent(LoginSuccess.this,CreateBill.class);
                    startActivity(intent_CreateBill);
                }
            });
        }

        if (btnTK != null) {
            btnTK.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent_TK = new Intent(LoginSuccess.this,BillStore.class);
                    startActivity(intent_TK);
                }
            });
        }
        if (btnCustomer != null) {
            btnCustomer.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent_Customer = new Intent(LoginSuccess.this,CustomerStore.class);
                    startActivity(intent_Customer);
                }
            });
        }
        if (btnProduct != null) {
            btnProduct.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent_Product = new Intent(LoginSuccess.this,ProductStore.class);
                    startActivity(intent_Product);
                }
            });
        }


    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_logout) {
            final AlertDialog.Builder builder = new AlertDialog.Builder(LoginSuccess.this);
            builder.setTitle(getResources().getString(R.string.request_logout))
                    .setPositiveButton(getResources().getString(R.string.yes), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            DBHelper db = new DBHelper(getApplication());
                            db.deleteState();
                            Intent intent_Login = new Intent(LoginSuccess.this,Login.class);
                            finish();
                            startActivity(intent_Login);
                        }
                    })
                    .setNegativeButton(getResources().getString(R.string.no), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            dialogInterface.dismiss();
                        }
                    }).show();
            return true;
        }
        if (id == R.id.action_refresh) {
            final AlertDialog.Builder builder = new AlertDialog.Builder(LoginSuccess.this);
            builder.setTitle(getResources().getString(R.string.request_close))
                .setPositiveButton(getResources().getString(R.string.yes), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        finish();
                    }
                })
                .setNegativeButton(getResources().getString(R.string.no), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                }).show();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
